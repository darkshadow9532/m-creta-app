class State extends Model {
    constructor(obj){
        super(obj, "/states");
    }
    filter_code(arr, code){
        let a = arr.filter((a)=>{
            return a.code == code;
        });
        return a[0];
    }
}