var app = angular.module("creta", []);
app.controller("customer", function($scope, $http, $interval, $timeout) {
    $scope.cretaCustomer = function(){
        var obj = {
            name: $scope.name,
            first: $scope.first,
            phone: $scope.phone,
            address: $scope.address,
            transport: $scope.transport,
            note: $scope.note
        }
        $http.post("/customer", obj).then(function(r){
            console.log(r);
            jQuery('#page-4').hide();
        })
    }
});
jQuery("#page-3").hide();
jQuery('#page-4').hide();
app.controller("show_customer", function($scope, $http, $interval, $timeout) {
    $scope.customers = [];
    $scope.customer;
    $scope.addnote = function(o){
        console.log(o);
        $scope.customer = o;
        jQuery("#page-3").show();
        jQuery("#a_note").focus();
    }
    $scope.syncnote = function(){
        console.log("TEST");
        $http.post("/anote", {
            obj: $scope.customer,
            note: $scope.a_note,
            id: $scope.customer.id
        }).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        jQuery("#page-3").hide();
        $scope.a_note = "";
        $scope.customer = {};
    }
    $scope.delete = function(id){
        console.log(id)
        $http.post("/delete",{id: id}).then((r)=>{
            console.log(r)
        }).catch((e)=>{
            console.log(e);
        })
    }
    $scope.getall = function(){
        $http.get("/acustomer").then((a)=>{
            console.log(a);
            $scope.customers = a.data.data;
        })
    }
    $scope.getall();
    $scope.page_customer = function(id){
        console.log(id);
        window.location.replace("/cus/" + id);
    }
});