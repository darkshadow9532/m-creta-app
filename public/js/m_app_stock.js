function formatNumber(num) {
    let temp;
    try{
        temp = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } catch (e) {
        temp = 0;
    }
    return temp;
}

app.controller("m_all_stock", async function($scope, $http, $interval, $timeout) {
    $scope.stocks = [];

    $scope.parseInt = parseInt;
    $scope.begin = async function(){
        let stock = new Stock();
        stock = await stock.find();
        $scope.$apply(()=>{
            $scope.stocks = stock;
            console.log($scope.stocks);
        });
    }
    await $scope.begin();
});