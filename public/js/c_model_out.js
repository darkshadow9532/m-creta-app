var NEW_ = 0;
var LOAD_ = 1;
function filter_array_key_value(arr, key, value){
    let s_arr = [];
    let i;
    for(i in arr){
        if(arr[i][key] == value){
            s_arr.push(arr[i]);
        }
    }
    return s_arr;
}

class ModelOut {
    constructor(obj, parent){
        this.url = parent;
        this.obj = obj;
        this.note = [];
        if((typeof obj) == "object"){
            this.status = NEW_;
        } else if (typeof obj == "string"){
            this.status = LOAD_;
            // this.begin();    
        } else {
            console.log("WARNING: using finding with " + parent)
        }
    }

    async begin(){
        if(this.status == LOAD_){
            let response = await fetch("/outs_one/" + this.obj, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    url: this.url
                })
            });
            let result = await response.json();
            console.log(result);
            if(result.status == 300){
                result = JSON.parse(result.data)
            } else {
                result = [];
            }
            this.obj = result;
        } else {
            /* NOTHING TO DO IN HERE */
        }
        
    }

    async save(){
        // console.log(this.obj);
        if(this.status == NEW_){
            // r_data = await post("/customers", this.obj);
            let response = await fetch("/outs", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    url: this.url,
                    data: this.obj
                })
            });
            let result = await response.json();
            console.log(result);
            if(result.status == 300){
                this.obj = JSON.parse(result.data);
            } else {

            }
            
            this.status = LOAD_;
            // return (result.message);
        } else {
            let response = await fetch("/outs/" + this.obj._id, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    url: this.url,
                    data: this.obj
                })
            });
            if(response.status == 300){
                this.obj = JSON.parse(response.data);
            } else {

            }
            
            // this.status = LOAD_;
        }
    }
/* 
        SYSTEM DEFINE SUPPORT FINDING A ARRAY ELEMENT IN SYSTEM MODEL
        YOU CANNOT CHANGE ANOTHING IN HERE
*/
    //  Find with attribute
    async find(obj){
        let response = await fetch("/outs_all", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({
                url: this.url
            })
        });
        let result = await response.json();
        if(result.status == 300){
            result = JSON.parse(result.data)
        } else {
            result = [];
        }
        let r_data = result;
        let i;
        for(i in obj){
            r_data = filter_array_key_value(r_data, i, obj[i]);               
        }
        return r_data;
    }

    async delete(){
        let a = await fetch("/outs_delete/" + this.obj._id,{
            method: "POST",
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({
                url: this.url
            })
        });
        let result = (await a.json());

        if(result.status == 300){
            result = result.data;
        } else {
            result = "";
        }
        // console.log(await a.json())
        // delete this.obj;
        return result;
    }

    get(){
        return this.obj;
    }

    set(o){
        this.obj = o;
    }

    getNote(){
        return this.note;
    }
    getDateString(){
        return (new Date(this.obj.createdAt)).toLocaleString();
    }
}

const url_customer = "https://thawing-taiga-94022.herokuapp.com/customers";
class Customer extends ModelOut {
    constructor(obj){
        super(obj, url_customer);
    }
}


const url_work = "https://secret-fjord-44684.herokuapp.com/works";
class Work extends ModelOut {
    constructor(obj){
        super(obj, url_work);
    }
}