function formatNumber(num) {
    let r;
    try{
        r = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } catch (e){
        r = "";
    }
    return r;
}

function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}

app.controller("m_all_controller", async function($scope,$http, $interval, $timeout){
    $scope.customers = [];
    $scope.customers_view = [];
    
    $scope.formatNumber = formatNumber;
    $scope.cutString = cutString;

    $scope.begin = async function(){
        let customer = new Customer();
        customer = await customer.find();
        // customer.forEach(async function(e, i){
        //     let j = new Customer(e._id);
        //     await j.begin();
        //     //customer[i].orders = await j.getOrder();
        //     //customer[i].total = await j.getTotal();
        //     $scope.$apply(()=>{
        //         $scope.customers_view = customer;
        //     })
        // });
        $scope.$apply(()=>{
            $scope.customers_view = customer;
        });        
    }
    $scope.analyze = function(customer){
        location.replace(window.location.origin + "/views/customer_explore/" + customer._id);
    }
    await $scope.begin();    
});

app.controller("m_analyze_controller", async function($scope, $http, $interval, $timeout){
    let url_parse = window.location.href.split("/");
    $scope.customer_id = url_parse[url_parse.length - 1];
    $scope.orders_all = [];
    $scope.orders_date = [];
    $scope.orders_product = [];
    

    $scope.begin = async function(){
        $scope.c_element = new Customer($scope.customer_id);
        await $scope.c_element.begin();
        let or = await $scope.c_element.getOrder();
        $scope.n_element = [];
        let tot = await $scope.c_element.getTotal();
        $scope.$apply(function(){
            $scope.orders = or;
            console.log(or);
            $scope.total = tot;
            $scope.d_element = $scope.c_element.get();
            // $scope.n_element = $scope.c_element.getNote();
            // $scope.total = $scope.getTotal();
        });
    }
    $scope.formatNumber = formatNumber;
    $scope.cutString = cutString;
    $scope.next = async function(){
        let dates = [];
        let moneys_by_date = [];
        let ors_by_date = [];
        for (let i in $scope.orders){
            $scope.orders_all[i] = $scope.orders[i].get();
            //console.log($scope.orders_all[i]);
            $scope.orders_all[i].sum = $scope.orders[i].getMoney();;
            $scope.orders_all[i].createdAt = moment($scope.orders_all[i].createdAt).format("YYYY-MM-DD");
        }
        //console.log("orders",$scope.orders_all);
        // order by date
        for (let i in $scope.orders_all){
            dates[i] = $scope.orders_all[i].createdAt;
        }
        //console.log(dates);
        //dates = ArrNoDupe(dates);
        //console.log(dates);
        for (let i in $scope.orders_all){
            for (let j in dates){
                if ($scope.orders_all[i].createdAt == dates[j]){
                    moneys_by_date[j] = moneys_by_date[j] + $scope.orders_all[i].sum || $scope.orders_all[i].sum;
                    ors_by_date[j] = ors_by_date[j] + 1 || 1;
                }
            }
        }
        $scope.$apply(function(){
            for ( let j in dates){
                $scope.orders_date[j] = {
                    dates: dates[j],
                    moneys: moneys_by_date[j],
                    ors: ors_by_date[j]
                }
            }
        })
        
        console.log($scope.orders_date);

        //product by customer
        let products = [];
        let amounts = [];
        let totals = [];
        let prices = [];
        let names = [];
        for (i in $scope.orders_all){
            for (j in $scope.orders_all[i].products){
                products.push($scope.orders_all[i].products[j].productId);
            }
        }
        products = ArrNoDupe(products);
        
        for (let i in $scope.orders_all){
            for  ( let j in $scope.orders_all[i].products){
                for (let k in products ){
                    if ( products[k] ==  $scope.orders_all[i].products[j].productId){
                        amounts[k] = amounts[k] + parseInt($scope.orders_all[i].products[j].amount) || parseInt($scope.orders_all[i].products[j].amount);
                        totals[k] = totals[k] + parseInt($scope.orders_all[i].products[j].amount) * ( parseInt($scope.orders_all[i].products[j].price) - parseInt($scope.orders_all[i].products[j].discount)) || parseInt($scope.orders_all[i].products[j].amount) * ( parseInt($scope.orders_all[i].products[j].price) - parseInt( $scope.orders_all[i].products[j].discount ));
                        names[k] = $scope.orders_all[i].products[j].name;
                    }
                }
            }
        }
        $scope.$apply(function(){
            for (k in products){
                prices[k] = Math.round(totals[k] / amounts[k]);
                $scope.orders_product[k] = {
                    name: names[k],
                    amount : amounts[k],
                    price : prices[k],
                    total : totals[k]
                }
                console.log($scope.orders_product);
            }
        })
        
    }

    await $scope.begin();
    await $scope.next();    
})