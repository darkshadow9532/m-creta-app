var orders;
var customers;
var products;
var impos;

app.controller("dashboard", function($scope, $http, $interval, $timeout) {
    $scope.orders;
    $scope.customers;
    $scope.products;
    $scope.impos;
    $scope.begin = function(){
        $http.get("/orders").then((a)=>{
            if(a.status == 200) {
                orders = a.data;
                $scope.orders = a.data;
            } else {
                console.log("GET /orders ERROR:", a.status);
            }
        })
        $http.get("/impos").then((a)=>{
            if(a.status == 200) {
                impos = a.data;
                $scope.impos = a.data;
            } else {
                console.log("GET /impos ERROR:", a.status);
            }
        })
        $http.get("/products").then((a)=>{
            if(a.status == 200) {
                products = a.data;
                $scope.status = a.data;
            } else {
                console.log("GET /products ERROR:", a.status);
            }
        })
        $http.get("/customers").then((a)=>{
            if(a.status == 200) {
                customers = a.data;
                $scope.customers = a.data;
            } else {
                console.log("GET /customers ERROR:", a.status);
            }
        })
    }


    /* RUNNING WHEN STARTUP */
    jQuery(document).ready(function(){
        $scope.begin();
    });
});