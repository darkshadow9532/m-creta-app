var orders;
var customers;
var products;
var impos;

let chart_data = [
    ['Jan',  7.0, -0.2, -0.9],
    ['Feb',  6.9, 0.8, 0.6],
    ['Mar',  9.5,  5.7, 3.5],
    ['Apr',  14.5, 11.3, 8.4],
    ['May',  18.2, 17.0, 13.5],
    ['Jun',  21.5, 22.0, 17.0],
    
    ['Jul',  25.2, 24.8, 18.6],
    ['Aug',  26.5, 24.1, 17.9],
    ['Sep',  23.3, 20.1, 14.3],
    ['Oct',  18.3, 14.1, 9.0],
    ['Nov',  13.9,  8.6, 3.9],
    ['Dec',  9.6,  2.5,  1.0]
 ];
function drawChart123() {
    // Define the chart to be drawn.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Ngày');
    // data.addColumn('number', 'Tokyo');
    // data.addColumn('number', 'Tokyo');
    data.addColumn('number', 'Doanh số');
    data.addRows(chart_data);
       
    // Set chart options
    var options = {'title' : 'Biểu đồ doanh số',
       hAxis: {
          title: 'Giao dịch'
       },
       vAxis: {
          title: 'Doanh số'
       },   
       'width': 1280,
       'height':720  
    };

    // Instantiate and draw the chart.
    var chart = new google.visualization.LineChart(document.getElementById('container'));
    chart.draw(data, options);
}
function drawChart(a){
    google.charts.setOnLoadCallback(drawChart123);
}

function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}

function orderAllTotal(os){
    let total = 0;
    for(i in os){
        total += orderToTotal(os[i]);
    }
    return total;
}

function idToCustomer(id){
    for(j in customers){
        if(customers[j]._id == id){
            return customers[j];
        }
    }
    return {};
}

function filterOrders(start, end){
        let startDay = start;
        let endDay = end;
        let os_f = [];
        for(i in orders){
            let t = (new Date(orders[i].createdAt)).getTime();
            if((t >= startDay) && (t <= endDay)){
                os_f.push(orders[i]);
            } else {
            }
        }
    return os_f;
}

function chartWithDay(start, end){
    let array = [];
    console.log(start, end);
    let i = 0;
    for(i = start + 86400000; i <= end;){
        console.log(i);
        let ods_f = filterOrders(i - 86400000, i);
        let date = new Date(i);
        array.push([date.getDate() + "/" + (date.getMonth() + 1), orderAllTotal(ods_f)]);
        i = i + 86400000;
    }
    return array;
}

app.controller("dashboard", function($scope, $http, $interval, $timeout) {
    $scope.orders;
    $scope.orders_f;
    $scope.customers;
    $scope.products;
    $scope.impos;
    $scope.begin = function(){
        $http.get("/orders").then((a)=>{
            if(a.status == 200) {
                orders = a.data;
                $scope.orders = a.data;
                $scope.orders_f = a.data;
            } else {
                console.log("GET /orders ERROR:", a.status);
            }
        })
        $http.get("/impos").then((a)=>{
            if(a.status == 200) {
                impos = a.data;
                $scope.impos = a.data;
            } else {
                console.log("GET /impos ERROR:", a.status);
            }
        })
        $http.get("/products").then((a)=>{
            if(a.status == 200) {
                products = a.data;
                $scope.status = a.data;
            } else {
                console.log("GET /products ERROR:", a.status);
            }
        })
        $http.get("/customers").then((a)=>{
            if(a.status == 200) {
                customers = a.data;
                $scope.customers = a.data;
            } else {
                console.log("GET /customers ERROR:", a.status);
            }
        })
    }

    $scope.searchOrder = function(){
        let startDay = (new Date($scope.startDay)).getTime();
        let endDay = (new Date($scope.endDay)).getTime();
        $scope.orders_f = filterOrders(startDay, endDay);
        chart_data = chartWithDay(startDay, endDay);
        console.log(chart_data);
        drawChart($scope.orders_f);
        jQuery("#chart").show()
    }

    /* FUNCTION SUPPORTED */
    $scope.orderToTotal = orderToTotal;
    $scope.idToCustomer = idToCustomer;
    $scope.orderAllTotal = orderAllTotal;
    /* SUPPORT */
    $scope.formatNum = formatNumber;
    $scope.date = date;
    /* RUNNING WHEN STARTUP */
    jQuery(document).ready(function(){
        $scope.begin();
        jQuery("#chart").hide();
    });
});