jQuery("#header_side").hide()
jQuery("#sidebar_side").hide();
jQuery("#black").hide();
jQuery("#counter").show();
function max(a,b){
    return a>b ? a:b;
}

app.controller("black", function($scope, $http, $interval, $timeout) {
    $scope.count_customer = 0;
    $scope.cus_first = {};
    $scope.timer = 0;
    let interval = 0;
    $scope.begin = async function(){
        $scope.timer = 0;
        interval = $interval(function(){
            $scope.timer += 1;
        }, 1000);
        let cus = new Customer();
        let count_customer = await cus.find();
        console.log(count_customer.length);
        let ord = new Order();
        ord = await ord.find();
        let total = 0;
        $scope.cus_first.money = 0;
        for(i in ord){
            let t_o = new Order(ord[i]._id)
            await t_o.begin();
            total += t_o.getMoney();
            $scope.cus_first.money = max($scope.cus_first.money, t_o.getMoney())
        }
        let im = new Impo();
        im = await im.find();
        let t_impo = 0;
        for(i in im){
            let t_i = new Impo(im[i]._id);
            await t_i.begin();
            console.log(t_i, t_i.getMoney());
            t_impo += t_i.getMoney();
        }
        $scope.$apply(function(){
            $scope.count_customer = count_customer.length;
            $scope.count_order = ord.length;
            $scope.count_total = total;
            $scope.count_sub = total - t_impo;
            jQuery("#black").show();
            jQuery("#counter").hide();
        });

    }
    $scope.draw = async function(){
        /* draw chart */
        let order = new Order();
        let ti = [];
        let da = [];
        console.log($scope.start);
        console.log($scope.end);
        let s = new Date($scope.start);
        let e = new Date($scope.end);
        let coun = ((e-s)/86400000);
        let i = 0;
        for(i = 0; i < coun; i++){
            console.log(s.getDate(), s.getMonth() + 1, s.getFullYear());
            let s_o = await order.find_date(s.getDate(), s.getMonth() + 1, s.getFullYear());
            ti.push(s.getDate() + "/" + (s.getMonth() + 1));
            da.push(s_o.length);
            s.setDate(s.getDate() + 1);
        }
        draw(ti, da, "myChart1"); 
    }
    $scope.draw_total = async function(){
        /* draw chart */
        let order = new Order();
        let ti = [];
        let da = [];
        console.log($scope.start);
        console.log($scope.end);
        let s = new Date($scope.start);
        let e = new Date($scope.end);
        let coun = ((e-s)/86400000);
        let i = 0;
        for(i = 0; i < coun; i++){
            console.log(s.getDate(), s.getMonth() + 1, s.getFullYear());
            let s_o = await order.find_date(s.getDate(), s.getMonth() + 1, s.getFullYear());
            let to_ = 0;
            let j = 0;
            for(j in s_o){
                let t_o = new Order(s_o[j]._id)
                await t_o.begin();
                to_ += t_o.getMoney();
            }
            ti.push(s.getDate() + "/" + (s.getMonth() + 1));
            da.push(to_);
            s.setDate(s.getDate() + 1);
        }
        draw(ti, da, "myChart");
    }
    $scope.formatNum = formatNumber;
    /* BEGIN */
    $scope.begin();
});

function draw(t, d, cv){
    var ctx = document.getElementById(cv).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: t,
            datasets: [{
                label: '# of Votes',
                data: d,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
}