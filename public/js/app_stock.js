var products;
function idToProduct(id){
    for(i in products){
        if(products[i]._id == id){
            return products[i];
        }
    }
    return {};
}
function formatNumber(num) {
    let temp;
    try{
        temp = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } catch (e) {
        temp = 0;
    }
    return temp;
  }

app.controller("all_stock", function($scope, $http, $interval, $timeout) {
    $scope.products = [];
    $scope.impos = [];
    $scope.orders = [];
    $scope.block = 0;
    $scope.stock = [];
    $scope.stock_filter = [];
    $scope.total_filter;
    $scope.filter_amount = function(num){
        let i;
        $scope.stock_filter = [];
        for(i in $scope.stock){
            if($scope.stock[i].amount < num){
                $scope.stock_filter.push($scope.stock[i]);
            }
        }

        // console.log($scope.stock_filter);
        let total = 0;
        for(i in $scope.stock_filter){
            console.log($scope.stock_filter[i].amount);
            try{
                let product = idToProduct($scope.stock_filter[i]._id);
                total += product.price * $scope.stock_filter[i].amount;
            } catch(e){
                console.log($scope.stock_filter[i]);
                // console.log(e);
                continue;
            }
        }
        // console.log($scope.stock_filter);
        // console.log()
        $scope.total_filter = total;
    }

    $scope.stock_total = function(){
        $scope.block += 1;
        if($scope.block < 3) {
            return;
        }
        var obj;
        for(i = 0; i < $scope.products.length; i++){
            obj = {};
            obj._id =  $scope.products[i]._id;
            obj.name = $scope.products[i].name;
            obj.amount = 0;
            obj.status = $scope.products[i].status
            // console.log(obj);    
            $scope.stock.push(obj);
            // console.log(obj);
        }
        console.log($scope.stock)
        for(i in $scope.impos){
            // console.log("ABC1", $scope.impos[i])
            for(j in $scope.impos[i].products){
                // console.log("ABC2")
                for(k in $scope.stock){
                    // console.log("ABC2")
                    if($scope.impos[i].products[j].productId == $scope.stock[k]._id){
                        // console.log("EFG", $scope.impos[i].products[j])
                        // console.log($scope.impos[i].products[j].amount)
                        $scope.stock[k].amount += parseInt($scope.impos[i].products[j].amount);
                        // console.log($scope.stock[k]);
                    }
                }
            }
        }
        //Add orders to STOCK
        for(i in $scope.orders){
            for(j in $scope.orders[i].products){
                for(k in $scope.stock){
                    if($scope.orders[i].products[j].productId == $scope.stock[k]._id){
                        $scope.stock[k].amount -= parseInt($scope.orders[i].products[j].amount);
                        // console.log($scope.stock[k]);
                    }
                }
            }
        }

    }
    $scope.formatNum = formatNumber;
    $http.get("/products").then((a)=>{
        $scope.products = a.data;
        $scope.stock_total();
        products = a.data;
    }).catch((e)=>{
        
    })
    $http.get("/impos").then((a)=>{
        $scope.impos = a.data;
        $scope.stock_total();
    }).catch((e)=>{
        
    })
    $http.get("/orders").then((a)=>{
        $scope.orders = a.data;
        $scope.stock_total();
    }).catch((e)=>{
        
    })
});