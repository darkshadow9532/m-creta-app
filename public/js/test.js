var ctx = document.getElementById('myChart').getContext('2d');
// var chart = new Chart(ctx, {
//     // The type of chart we want to create
//     type: 'line',

//     // The data for our dataset
//     data: {
//         labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//         datasets: [{
//             label: 'My First dataset',
//             // backgroundColor: 'rgb(255, 255, 255)',
//             borderColor: 'rgb(255, 99, 132)',
//             data: [0, 10, 5, 2, 20, 30, 45]
//         }]
//     },

//     // Configuration options go here
//     options: {}
// });

// fx = 2*x+3;
let y=['Running', 'Swimming', 'Eating', 'Cycling'];
let x=[10, 10, 8, 12];
// for(i = -20; i < 20; i++){
//     x.push(i.toString());
//     let yx = - 3*i*i - 30*i + 9;
//     y.push(yx);
// }
var Chart = new Chart(ctx, {
    type: "bubble",
    data:{
        labels: y,
        datasets: [{
            data: [10, 10, 4, 2],
            // backgroundColor: fillPattern
        }]
    },
    options: {
        responsive: false,
        maintainAspectRatio: false,
        aspectRatio: 1,
        tooltips: {
            mode: 'nearest',
            intersect: false,
            backgroundColor: 'rgba(0,0,0,1)',
            cornerRadius: 20,
            // borderColor: 'rgba()'
        },
        title: {
            display: true,
            text: 'Custom Chart Title'
        },
        elements:{
            point: {
                radius: 4,
                borderColor: 'rgba(255,0,0,0.7)',

            },
            line:{
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(255,0,0,0.5)',
                // tension: 2,
                borderWidth: 1
            }
        }
    }
})