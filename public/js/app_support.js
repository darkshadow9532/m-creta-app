var products;
function getAllProducts(){
  jQuery.get("/products", function(data){
    // console.log(data);
    products = data;
  })
}

function getProducts(){
  return products;
}
function getProductById(id){
  for(i in products){
    if(products[i]._id == id){
      return products[i];
    }
  }
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function cutString(str, n){
  var b;
  try{
    b = str.length;
  }  catch (e){

  }
  let a;
  try{
    a = str.slice(b-n, b);
  } catch (e){

  }
  return a;
}

function date(str){
  var a = new Date(str);
  return a.toLocaleDateString();
}
// getAllProducts();

var clr = "";
function runNotify(content, color, timeout_second){
  var dNotify = jQuery("#notify");
  var pNotify = jQuery("#ntfContent");
  if(dNotify.hasClass(clr)){
    dNotify.removeClass(clr);
  }
  clr = "w3-" + color;
  dNotify.addClass(clr);
  pNotify.text(content);
  dNotify.show();
  setTimeout(function(){
    dNotify.removeClass(clr);
    dNotify.hide();
  }, timeout_second * 1000);
}