class Customer extends Model {
    constructor(obj){
        super(obj,"/customers")
    }
    async getOrder(){
        if(this.status == LOAD_){
            let i;
            let p = new Order();
            let a_p = await p.find({customer: this.obj._id});
            p = [];
            for(i in a_p){
                let or = new Order(a_p[i]._id);
                await or.begin();
                p.push(or);
            }
            return p;
        } else {
            return [];
        }
    }

    async getTotal(){
        let or = await this.getOrder();
        console.log(or);
        let total = 0;
        or.forEach((a)=>{
            total += a.getMoney();
        })
        return total;
    }
}