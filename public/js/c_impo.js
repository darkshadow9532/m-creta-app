function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}

class Impo extends Model {
    constructor(obj){
        super(obj, "/impos");
    }
    getMoney(){
        return orderToTotal(this.obj);
    }
}