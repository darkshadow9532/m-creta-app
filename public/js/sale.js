var _customers = [];
function id_to_name(id){
    let _t_customer = _customers.find(function(a){
        return a._id == id;
    })
    return _t_customer.name;
}

async function _begin(){
    let cus = new Customer();
    _customers = await cus.find();
}
var app = angular.module("person", []);
app.controller("sale", async function($scope, $http, $interval, $timeout) {
    await _begin();
    let o = new Order();
    let orders = await o.find();
    let s = new State();
    let states = await s.find();
    let a = s.filter_code(states, "order_controller");
    $scope.cutString = function(str, n){
        var b;
        // console.log(str, n)
        try{
            b = str.length;
        } catch(e){
            return;
            console.log(e);
        }
         
        return str.slice(b-n, b);
    }
    $scope.state_to_string = function(step){
        let t = a.state.find(function(e){
            return e.step == step;
        })
        return t;
    }
    $scope.detail = function(i){
        location.replace(window.location.origin + "/views/order/" + i._id);
    }
    $scope.id_to_name_customer = async function(id){
        await id_to_name_customer(id);
    };
    $scope.$apply(function(){
        $scope.run_orders = orders.filter(function(e){
            return e.status != "5";
        })
        console.log($scope.run_orders);
    })
});