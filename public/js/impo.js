// function numberfin(num){
//     return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(num)
// }
// var app = angular.module("order", []);
app.controller("create_impo", function($scope, $http, $interval, $timeout) {
    $scope.products = [];
    $scope.aproducts = [];
    $scope.aproduct;
    $scope.customers = [];
    $scope.customer = "";
    $scope.staffs = [];
    $scope.staff = "";
    $scope.date = "";
    // $scope.create = function(){

    // }
    $http.get("/products").then((a)=>{
        // console.log(a);
        $scope.aproducts = a.data;
    })
    $scope.addProduct = function(){
        if($scope.aproduct == "" || $scope.aproduct == undefined){
            alert("Chọn sản phẩm trước khi thêm nhé!");
            return;
        }
        console.log($scope.aproduct);
        let p = JSON.parse($scope.aproduct);
        $scope.products.push({
            productId: p._id,
            name: p.name,
            amount: 1,
            price_in: p.price_in,
            price: p.price,
            discount: 0
        });
        // console.log($scope.aproduct);
        // if($scope.aproduct == "" || $scope.aproduct == undefined){
        //     alert("Chọn sản phẩm trước khi thêm nhé!");
        //     return;
        // }
        // // let temp = $scope.aproduct.split("---")[0];
        // // for(i in $scope.aproducts){
        //     // if($scope.aproducts[._id == temp){
        //         $scope.products.push({
        //             productId: $scope.aproduct._id,
        //             name: $scope.aproduct.name,
        //             amount: 1,
        //             price: $scope.aproduct.price,
        //             discount: 0
        //         });
            // }    
        // }
        // console.log("HEREx")
    }

    $scope.totalProductsOut = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    $scope.totalProductsIn = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price_in*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    // customer: String,
    // staff: String,
    // produtcs: Array,
    $scope.create = function(){
        
        var obj = {
            vendor: $scope.vendor,
            staffId: $scope.staff._id,
            products: $scope.products,
            date: $scope.date
        };
        $http.post("/impos", obj).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        for (i in obj.products){
            $http.get("")
        }
    }

    $http.get("/customers").then((a)=>{
        // console.log(a);
        $scope.customers = a.data;
    }).catch((e)=>{
        console.log(e);
    })
    $http.get("/staffs").then((a)=>{
        // console.log(a);
        $scope.staffs = a.data;
    }).catch((e)=>{
        console.log(e);
    })
    $scope.addCustomer = function(){
        // console.log($scope.customer)
    }
});

app.controller("all_impo", function($scope, $http, $interval, $timeout) {
    $scope.impos = [];
    $scope.detail_impo = {};
    $scope.cutString = function(str, n){
        var b = str.length; 
        return str.slice(b-n, b);
    }
    $scope.totalProducts = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    $scope.getTotalIn = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price_in) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.getTotalOut = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.detail = function(i){
        $scope.detail_impo = i;
        // if($scope.detail_impo.finance == undefined){
        //     $scope.detail_impo = false;
        // }
        console.log(i)
        jQuery("#detail_all_impo").show();
        jQuery("#all_impo").hide();
    }

    $scope.back = function(){
        jQuery("#detail_all_impo").hide();
        jQuery("#all_impo").show();
    }

    $scope.delete = function(){
        if(confirm("Bạn đang xoá một đơn nhập hàng?")){
            $http.delete("/impos/" + $scope.detail_impo._id).then((a)=>{
                runNotify("Bạn đã xoá đơn nhập hàng", "green", 5);
                $scope.begin();
            }).catch((e)=>{
                runNotify("Có một lỗi xảy ra", "red", 5);
            })
            $http.post("/webhooks/impo/finance/DELETE", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        } else {

        }
    }
    $scope.checkFinance = function(){
        console.log("FUN:", $scope.detail_impo.finance);
        if($scope.detail_impo.finance == true){
            if(confirm("Bạn có muốn ra một thông báo!")){
                console.log("Chúc mừng bạn!");
            } else {
                console.log("Cố gắng lên nha!");
            }
            $http.post("/webhooks/impo/finance/PAY", $scope.detail_impo).then((a)=>{
                console.log(a.data);
            })
        } else if ($scope.detail_impo.finance == false){
            $http.post("/webhooks/impo/finance/DELETE_CREATE", $scope.detail_impo).then((a)=>{
                console.log(a.data);
            })
        } else {
            $scope.detail_impo.finance = false;
        }
        $scope.edit();
    }
    $scope.edit = function(){
        if(confirm("Bạn đang cập nhật thông tin đơn nhập!.")){
            $http.put("/impos/" + $scope.detail_impo._id, $scope.detail_impo).then((a)=>{
                runNotify("Bạn đã cập nhật đơn nhập hàng", "green", 5);
            }).catch((e)=>{
                runNotify("Có lỗi xảy ra trong quá trình cập nhật", "red", 5);
            })
        } else {

        }
    }
    $scope.formatNumber = formatNumber;
    $scope.date = date;
    $scope.cutString = cutString;
    $scope.begin = function(){
        $http.get("/impos").then((a)=>{
            if(a.status == 200){
                $scope.impos = a.data;
            }
        }).catch((E)=>{
            console.log(E);
        })
    }

    //
    $scope.begin();
    jQuery("#detail_all_impo").hide();
});