var products = [];
var detail_order_source = {};
var allProducts = [];

app.controller("create_order", function($scope, $http, $interval, $timeout) {
    $scope.products = [];
    $scope.aproduct;
    $scope.customers = [];
    $scope.customer = "";
    $scope.staffs = [];
    $scope.staff = "";
    $scope.state_order = "";
    // $scope.detail_all_order = "";
    $scope.aproducts = [];
    $scope.loadProducts = function(){
        $http.get("/products").then((a)=>{
            $scope.aproducts = a.data;
        })
    }
    $http.get("/states?code=order_controller").then((a)=>{
        if(a.status == 200){
            $scope.state_order = a.date[0];
        }
    }).catch((e)=>{

    })
    $scope.addProduct = function(){
        if($scope.aproduct == "" || $scope.aproduct == undefined){
            alert("Chọn sản phẩm trước khi thêm nhé!");
            return;
        }
        console.log($scope.aproduct);
        let p = JSON.parse($scope.aproduct);
        products.push(p);
        $scope.products.push({
            productId: p._id,
            name: p.name,
            amount: 1,
            price: p.price,
            discount: 0
        });
    }
    $scope.getStateOrder = function(step){
        for(i in $scope.state_order.state){
            if($scope.state_order.state[i].step == step){
                return $scope.state_order.state[i].name;
            }
        }
    }
    $scope.totalProducts = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price*$scope.products[i].amount - $scope.products[i].discount);
        }
        return (total);
    }
    // customer: String,
    // staff: String,
    // produtcs: Array,
    $scope.create = function(){
        console.log($scope.customer);
        if($scope.products.length == 0){
            runNotify("Chúng ta bán món gì nào?", "yellow", 5);
            return;
        }
        if($scope.customer == ""){
            runNotify("Bạn phải biết mình bán cho ai chứ nhỉ?", "yellow", 5);
            return;
        }
        
        var obj = {
            customerId: $scope.customer._id,
            staffId: $scope.staff._id,
            products: $scope.products,
            status: 1
        };
        $http.post("/orders", obj).then((a)=>{
            // console.log(a);
            if(a.status == 200){
                $scope.customer = "";
                $scope.staff = "";
                let obj_product = {}
                for (let j in products){
                    obj_product = {
                        name: products[j].name,
                        detail: products[j].detail,
                        price_in: products[j].price_in,
                        price: products[j].price,
                        amount: parseInt(products[j].amount) - parseInt($scope.products[j].amount),
                        version: products[j].version,
                        vendor: products[j].vendor,
                        image: products[j].image,
                        code: products[j].code,
                        sku: products[j].sku,
                        status: products[j].status                        
                    }
                    $http.put("/products/"+ products[j]._id, obj_product).then(function(res){
                        console.log(res);
                    })
                    .catch((er)=>{
                        console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                        console.log(er);
                    })
                }
                $scope.products = [];
                products = [];
                runNotify("Tạo thành công đơn hàng: " + a.data._id, "green", 5);
                // jQuery("#btnReloadDetailAllOrder").click();
            } else {
                runNotify("Tạo đơn hàng thất bại", "red", 5);
            }
        }).catch((e)=>{
            console.log(e);
        })
    }
    $scope.loadCustomers = function(){
        $http.get("/customers").then((a)=>{
            // console.log(a);
            $scope.customers = a.data;
        }).catch((e)=>{
            // console.log(e);
        })
    }
    
    $http.get("/staffs").then((a)=>{
        // console.log(a);
        $scope.staffs = a.data;
    }).catch((e)=>{
        // console.log(e);
    })
    $scope.addCustomer = function(){
        // console.log($scope.customer)
    }

    //Beginner
    $scope.loadCustomers();
    $scope.loadProducts();
});


var sortNames = function(arr){

    arr = arr.sort(function(a,b){
      return a.name > b.name;
    })
  
    for (var i = 0; i < arr.length; i++){
      if (arr[i].childs){
        sortNames(arr[i].childs)
      }
    }
    return arr;
  }
  var names = [
      {
          "name": "Root1",
          "Id": "2f3d17cb-d9e2-4e99-882d-546767f2765d",
          "status": "",
          "dispName": "",
          "imageURL": "",
          "childCount": "",
          "childs": [
              {
                  "name": "Sub1",
                  "Id": "ff8b3896-3b80-4e1b-be89-52a82ec9f98f",
                  "childs": [
                      {
                          "name": "Template1",
                          "Id": "ff8b3896-3b80-4e1b-be89-52a82ec9f981",
                          "status": "",
                          "dispName": "",
                          "imageURL": ""
                      },
                      {
                          "name": "Template2",
                          "Id": "ff8b3896-3b80-4e1b-be89-52a82ec9f982",
                          "status": "",
                          "dispName": "",
                          "imageURL": ""
                      }
                  ]
              },
              {
                  "name": "Template3",
                  "Id": "ff8b3896-3b80-4e1b-be89-52a82ec9f981"
  
              }
          ]
      },
      {
          "name": "Root2",
          "Id": "ea0586e7-02cf-4359-94ba-8d9623590dfe",
          "childs": [
              {
                  "name": "Sub2",
                  "Id": "6f1b3a60-d295-413e-92ef-1c713446e6c9",
                  "childs": [
                      {
                          "name": "Template4",
                          "Id": "6f1b3a60-d295-413e-92ef-1c713446e6c1"
                      },
                      {
                          "name": "Template5",
                          "Id": "6f1b3a60-d295-413e-92ef-1c713446e6c2"
                      }
                  ]
              }
          ]
      }
  ];
  
sortNames(names);

app.controller("all_order", function($scope, $http, $interval, $timeout) {
    $scope.orders = [];
    $scope.customers = [];
    $scope.staffs = [];
    $scope.detail_order = {};
    $scope.detail_products = [];
    $scope.detail_notes = [];
    $scope.detail_customer = {};
    $scope.state_order = {};

    

    $scope.PO = function(id){
        let temp = "PO" + $scope.cutString("000000" + id.toString(), 5);
        return temp;
    }

    $scope.cutString = function(str, n){
        var b;
        try{
            b = str.length;
        } catch(e){
            return;
            console.log(e);
        }
         
        return str.slice(b-n, b);
    }
    $scope.date = date;

    $scope.begin = function (){
        $http.get("/orders").then((a)=>{
            // console.log(a);
            if(a.data)
                $scope.orders = a.data;
                $scope.orders.reverse();
                $scope.orders_view = $scope.orders;
        }).catch((e)=>{
    
        })

        $http.get("/works").then(function(res){
            if(res.data){
                $scope.works = res.data;
            }
        })
        $http.get("/states?code=order_controller").then((a)=>{
            if(a.status == 200){
                // console.log(a.data);
                $scope.state_order = a.data[0];
                // console.log("123",scope.state_order);
            }

        }).catch((e)=>{

        })
    }
    $http.get("/customers").then((a)=>{
        // console.log(a);
        if(a.data)
            $scope.customers = a.data;
    }).catch((e)=>{

    })
    $scope.getCustomerName = function(customerId){
        // console.log(customerId);
        for(i in $scope.customers){
            if($scope.customers[i]._id == customerId){
                return $scope.customers[i].first + " " + $scope.customers[i].name;
            }
        }
        return "Không xác định";
    }
    $scope.getTotal = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.getTotalI = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return (temp);
    }
    $scope.detail = function(i){
        console.log(i);
        $scope.detail_order = i;
        for(j in $scope.customers){
            if($scope.customers[j]._id == i.customer){
                $scope.detail_customer = $scope.customers[j];
            }
        }
        $http.get("/orders/"+i._id).then(function(res){
            detail_order_source = res.data;
        })
        $http.get("/products/").then(function(res){
            allProducts = res.data;
        });
        $scope.search_note();
        jQuery("#all_order").hide();
        jQuery("#detail_all_order").show();
        
    }
    $scope.getStateOrder = function(step){
        // console.log("STATE:", step);
        // console.log($scope.state_order);
        for(i in $scope.state_order.state){
            if($scope.state_order.state[i].step == step){
                return $scope.state_order.state[i].name;
            }
        }
        return "Chưa có trạng thái";
    }
    $scope.back = function(id){
        jQuery("#all_order").show();
        jQuery("#" + id).hide();
    }
    $scope.deleteOrder = function(id){
        if(confirm("Bạn đang dự định xoá đơn hàng: " + id + "?")){

        } else {
            return;
        }
        let obj_product = {};
        for(i in $scope.detail_order.products){
            for (j in allProducts){
                if( allProducts[j]._id == $scope.detail_order.products[i].productId){
                    obj_product = {
                        name: allProducts[j].name,
                        detail: allProducts[j].detail,
                        price_in: Math.round( ( ( parseInt(allProducts[j].price_in) *  parseInt(allProducts[j].amount) ) + ( parseInt($scope.detail_order.products[i].price_in) * parseInt($scope.detail_order.products[i].amount) ) )/( parseInt(allProducts[j].amount) + parseInt($scope.detail_order.products[i].amount) ) ),
                        price: allProducts[j].price,
                        amount: parseInt(allProducts[j].amount) + parseInt($scope.detail_order.products[i].amount),
                        version: allProducts[j].version,
                        vendor: allProducts[j].vendor,
                        image: allProducts[j].image,
                        code: allProducts[j].code,
                        sku: allProducts[j].sku,
                        status: allProducts[j].status                
                    }
                    $http.put("/products/"+ allProducts[j]._id, obj_product).then(function(res){
                        console.log(res);
                    })
                }
            }
            
        }
        $http.delete("/orders/" + id).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        $http.post("/webhooks/order/finance/DELETE", $scope.detail_order).then((a)=>{
            console.log(a.data);
        })
        $scope.begin();
        $scope.back('detail_all_order');
    }

    $scope.createWork = function(time){
        console.log($scope.detail_order)
        let req = {
            method: "POST",
            url: "/works",
            header: {
                'Content-Type': "application/json"
            },
            data: {
                action: "",
                time: time,
                parent: "orders",
                parentId: $scope.detail_order._id,
                name: $scope.detail_order.status,
                message: $scope.state_order.state[$scope.detail_order.status].name
            }
        }
        console.log(req);
        $http(req).then(function(res){
            console.log(res);
            if (true){
                
            }
        })
        .catch((er)=>{
            console.log(er);
        })
    }
    $scope.updateOrder = function(){
        console.log($scope.detail_order)
        $http.put("/orders/" + $scope.detail_order._id, $scope.detail_order).then((a)=>{
            if(a.status == 200){
                runNotify("Cập nhật thành công đơn hàng " + a.data._id, "green", 5);
            } else {
                runNotify("Thất bại cập nhật đơn", "red", 5);
            }
        }).catch((e)=>{
            runNotify("Thất bại cập nhật đơn", "red", 5);
        })
        let obj_product = {}
        for (i in $scope.detail_order.products){
            for (j in allProducts){
                if( allProducts[j]._id == $scope.detail_order.products[i].productId){
                    //console.log("all", allProducts[j].amount);
                    //console.log("now", $scope.detail_impo.products[i].amount);
                    //console.log("source", detail_impo_source.products[i].amount);
                    obj_product = {
                        name: allProducts[j].name,
                        detail: allProducts[j].detail,
                        price_in: Math.round( ( ( parseInt(allProducts[j].price_in) *  parseInt(allProducts[j].amount) ) - ( parseInt($scope.detail_order.products[i].price_in) * parseInt($scope.detail_order.products[i].amount) ) + (parseInt(detail_order_source.products[i].price_in) * parseInt(detail_order_source.products[i].amount) ) )/( parseInt(allProducts[j].amount) - parseInt($scope.detail_order.products[i].amount) + parseInt(detail_order_source.products[i].amount) ) ),
                        price: allProducts[j].price,
                        amount: parseInt(allProducts[j].amount) - parseInt($scope.detail_order.products[i].amount) + parseInt(detail_order_source.products[i].amount),
                        version: allProducts[j].version,
                        vendor: allProducts[j].vendor,
                        image: allProducts[j].image,
                        code: allProducts[j].code,
                        sku: allProducts[j].sku,
                        status: allProducts[j].status                        
                    }
                    //console.log("Total:", obj_product.amount);
                    $http.put("/products/"+ allProducts[j]._id, obj_product).then(function(res){
                        console.log(res);
                    })
                    .catch((er)=>{
                        console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                        console.log(er);
                    })
                }

            }
        }
    }

    $scope.add_note = function(){
        jQuery("#order_note").show();
        setIdNoteOrder($scope.detail_order._id);
    }

    $scope.search_note = function(){
        $http.get("/search/notes?sourceId=" + $scope.detail_order._id + "&source=orders").then((a)=>{
            // console.log(a);
            $scope.detail_notes = a.data;
        }).catch((e)=>{
            console.log(e);
        })
    }

    $scope.print= function(){
        printJS({ 
            printable: 'order-form', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.printStock = function(){
        printJS({ 
            printable: 'print-check-product', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.printTransport = function(){
        printJS({ 
            printable: 'transport-form', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.define_controller_fin = "5d9e98955b547c0c297868b3"; //id of ifttt
    $scope.define_status_fin = [
        {
            name: "Đã thanh toán",
            code: "da_thanh_toan"
        },
        {
            name: "Chưa thanh toán",
            code: "chua_thanh_toan"
        }
    ]
    $scope.finance = function(){
        var state_fin = 0;
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                // return;
                state_fin = 1;
            }
        }
        if(state_fin == 0){
            $scope.detail_order.total = $scope.getTotalI($scope.detail_order);
            $scope.detail_order.parent = "BAN_SI";
            $scope.detail_order.info_customer = "Đơn hàng lẻ: " + $scope.getCustomerName($scope.detail_order.customer) + " - " + $scope.detail_order._id;
            $http.post("/fin/create", $scope.detail_order).then((a)=>{
                $scope.detail_order.extend.push({
                    name: "finance",
                    idB: a.data._id,
                    _id: $scope.define_controller_fin
                });
                console.log(a);
            }).catch((e)=>{
                console.log(e);
            })
        } else if(state_fin == 1){
            $scope.updateOrder();
            let extend = {};
            console.log($scope.detail_order)
            for(i in $scope.detail_order.extend){
                if($scope.detail_order.extend[i].name == "finance"){
                    extend = $scope.detail_order.extend[i];
                }
            }
            extend.oA = {
                status: $scope.detail_order.status
            }
            $http.post("/fin/update", extend).then((a)=>{

            }).catch((e)=>{

            })
        }
    }
    $scope.definance = function(){
        var a = {};
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                a = $scope.detail_order.extend[i];
            }
        }
        $http.post("/fin/delete", a).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                // a = $scope.detail_order.extend[i];
                $scope.detail_order.extend.splice(i, 1);
                break;
            }
        }
    }

    $scope.deleteProduct = function(i){
        // console.log(i);
        if(confirm("Bạn đang xoá: " + i.name + "!. Quá trình này không thể phục hồi!. Tiếp tục")){

        } else {
            return;
        }
        for(j in $scope.detail_order.products){
            if($scope.detail_order.products[j].productId == i.productId){
                // console.log(i.productId);
                $scope.detail_order.products.splice(j, 1);
                break;
            }
        }
    }

    $scope.aproducts = [];
    $scope.loadProducts = function(){
        $http.get("/products").then((a)=>{
            $scope.aproducts = a.data;
        })
    }
    $scope.addProduct = function(){
        if($scope.aproduct == "" || $scope.aproduct == undefined){
            alert("Chọn sản phẩm trước khi thêm nhé!");
            return;
        }
        console.log($scope.aproduct);
        let p = JSON.parse($scope.aproduct);
        $scope.detail_order.products.push({
                    productId: p._id,
                    name: p.name,
                    amount: 1,
                    price: p.price,
                    discount: 0
                });
                jQuery("#id01").hide();
    }
    $scope.loadProducts();

    /* STATE */
    $scope.pre_state = function(){
        /* Các STEP không thể lùi lại của đơn hàng */
        if(($scope.detail_order.status == 3)){
            alert("Xin lỗi, trạng thái này không khả dụng!.");
            return;
        }
        if(confirm("Bạn muốn lùi trạng thái của đơn hàng này?")){

        } else {
            return;
        }
        if(parseInt($scope.detail_order.status) > 1){
            $scope.detail_order.status = parseInt($scope.detail_order.status) - 1;
        } else {
            $scope.detail_order.status = 1;
        }
        $scope.updateOrder();
        $scope.createWork(1);
    }

    $scope.nex_state = function(){
        for(i in $scope.state_order.state){
            if($scope.detail_order.status == $scope.state_order.state[i].step){
                if($scope.state_order.state[i].completed){
                    alert("Đơn hàng này đã giao thành công!. Đừng phá hệ thống TÀO LAO.");
                    return;
                }
            }  
        }
        if(confirm("Bạn muốn đẩy tới trạng thái của đơn hàng này?")){

        } else {
            return;
        }
        if(parseInt($scope.detail_order.status) >= 1){
            $scope.detail_order.status = parseInt($scope.detail_order.status) + 1;
        } else {
            $scope.detail_order.status = 1;
        }

        if($scope.detail_order.status == 3){
            if(confirm("Bạn đã sẵn sàng!, bước này sẽ gởi thông tin đến giao vận và tài chính xác nhận hoá đơn. Chúng ta không thể lùi lại!")){
                /* WEBHOOK */
                $http.post("/webhooks/order/finance/DEBT", $scope.detail_order).then((a)=>{
                    console.log(a.data);
                })
            } else {
                return;
            }
        } else if ($scope.detail_order.status == 5) {
            alert("Chúc mừng bạn một đơn hàng đã thành công");
        }
        $scope.updateOrder();
        $scope.createWork(1);
    }

    /* CHECKBOX */
    $scope.checkFinance = function(){
        if($scope.detail_order.finance){
            if(confirm("Bạn có muốn ra một thông báo!")){
                console.log("Chúc mừng bạn!");
            } else {
                console.log("Cố gắng lên nha!");
            }
            $http.post("/webhooks/order/finance/PAY", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        } else {
            $http.post("/webhooks/order/finance/DELETE_CREATE", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        }
        $scope.updateOrder();
    }

    $scope.after_begin = function(){
        let now = moment();
        console.log($scope.orders_view);
        console.log($scope.works);
        for (i in $scope.orders){
            for (j in $scope.works){
                if ( ($scope.orders[i]._id == $scope.works[j].parentId) && ( $scope.orders[i].status == $scope.works[j].name)){
                    console.log("casc");
                    if(moment($scope.works[j].time).isBefore(now)){
                        $scope.$apply(function(){
                            $scope.orders_view.alert == true;
                            console.log("Minh");
                        });
                        
                    }
                }
            }
        }
    }
    // $scope.reload()
    $scope.formatNum = formatNumber;
    /* RUN WHEN STARTUP */
    $scope.begin();
    $timeout( function(){ $scope.after_begin() },2000);
    $scope.back('detail_all_order');
    jQuery("#all_order").show();
    jQuery("#detail_all_order").hide();
});

app.controller("show_order", async function($scope, $http, $interval, $timeout) {
    $scope.orders = [];
    $scope.orders_view = [];
    $scope.customers = [];
    $scope.staffs = [];
    $scope.detail_order = {};
    $scope.detail_products = [];
    $scope.detail_notes = [];
    $scope.detail_customer = {};
    $scope.state_order = {};

    $scope.PO = function(id){
        let temp = "PO" + $scope.cutString("000000" + id.toString(), 5);
        return temp;
    }

    $scope.cutString = function(str, n){
        var b;
        try{
            b = str.length;
        } catch(e){
            return;
            console.log(e);
        }
         
        return str.slice(b-n, b);
    }
    $scope.date = date;

    $scope.begin = async function (){
        $http.get("/orders").then((a)=>{
            // console.log(a);
            if(a.data)
                $scope.orders = a.data;
                $scope.orders.reverse();
                $scope.orders_view = $scope.orders;
        }).catch((e)=>{
    
        })
        $http.get("/states?code=order_controller").then((a)=>{
            if(a.status == 200){
                $scope.state_order = a.data[0];
            }

        }).catch((e)=>{

        })
        let u = window.location.href.split("/");
        let i = 0;
        $scope.element;
        for(i in u){
            $scope.element = u[i];
        }
        let or = new Order(u[i]);
        await or.begin();
        $scope.$apply(function(){
            $scope.detail_order = or.get();
            console.log($scope.detail_order)
        });
        
    }
    // $scope.begin();
    $http.get("/customers").then((a)=>{
        // console.log(a);
        if(a.data)
            $scope.customers = a.data;
    }).catch((e)=>{

    })
    $scope.getCustomerName = function(customerId){
        // console.log(customerId);
        for(i in $scope.customers){
            if($scope.customers[i]._id == customerId){
                return $scope.customers[i].first + " " + $scope.customers[i].name;
            }
        }
        return "Không xác định";
    }
    $scope.getTotal = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.getTotalI = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return (temp);
    }

    $scope.customer = function(){
        console.log("A")
        location.replace(window.location.origin + "/views/customer/" + $scope.detail_order.customer)
    }

    $scope.detail = function(i){
        console.log(i);
        $scope.detail_order = i;
        for(j in $scope.customers){
            if($scope.customers[j]._id == i.customer){
                $scope.detail_customer = $scope.customers[j];
            }
        }
        $http.get("/orders/"+i._id).then(function(res){
            detail_order_source = res.data;
        })
        $http.get("/products/").then(function(res){
            allProducts = res.data;
        });
        $scope.search_note();
        jQuery("#all_order").hide();
        jQuery("#detail_all_order").show();
        
    }
    $scope.getStateOrder = function(step){
        // console.log("STATE:", step);
        // console.log($scope.state_order);
        for(i in $scope.state_order.state){
            if($scope.state_order.state[i].step == step){
                return $scope.state_order.state[i].name;
            }
        }
        return "Chưa có trạng thái";
    }
    $scope.back = function(id){
        jQuery("#all_order").show();
        jQuery("#" + id).hide();
    }
    $scope.deleteOrder = function(id){
        if(confirm("Bạn đang dự định xoá đơn hàng: " + id + "?")){

        } else {
            return;
        }
        let obj_product = {};
        for(i in $scope.detail_order.products){
            for (j in allProducts){
                if( allProducts[j]._id == $scope.detail_order.products[i].productId){
                    obj_product = {
                        name: allProducts[j].name,
                        detail: allProducts[j].detail,
                        price_in: Math.round( ( ( parseInt(allProducts[j].price_in) *  parseInt(allProducts[j].amount) ) - ( parseInt($scope.detail_impo.products[i].price_in) * parseInt($scope.detail_impo.products[i].amount) ) )/( parseInt(allProducts[j].amount) - parseInt($scope.detail_impo.products[i].amount) ) ),
                        price: allProducts[j].price,
                        amount: parseInt(allProducts[j].amount) - parseInt($scope.detail_impo.products[i].amount),
                        version: allProducts[j].version,
                        vendor: allProducts[j].vendor,
                        image: allProducts[j].image,
                        code: allProducts[j].code,
                        sku: allProducts[j].sku,
                        status: allProducts[j].status                        
                    }
                    $http.put("/products/"+ allProducts[j]._id, obj_product).then(function(res){
                        console.log(res);
                    })
                }
            }
            
        }
        $http.delete("/orders/" + id).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        $http.post("/webhooks/order/finance/DELETE", $scope.detail_order).then((a)=>{
            console.log(a.data);
        })
        $scope.begin();
        
        
        $scope.back('detail_all_order');
    }

    $scope.updateOrder = function(){
        console.log($scope.detail_order)
        $http.put("/orders/" + $scope.detail_order._id, $scope.detail_order).then((a)=>{
            if(a.status == 200){
                runNotify("Cập nhật thành công đơn hàng " + a.data._id, "green", 5);
            } else {
                runNotify("Thất bại cập nhật đơn", "red", 5);
            }
        }).catch((e)=>{
            runNotify("Thất bại cập nhật đơn", "red", 5);
        })
        let obj_product = {}
        for (i in $scope.detail_impo.products){
            for (j in allProducts){
                if( allProducts[j]._id == $scope.detail_impo.products[i].productId){
                    //console.log("all", allProducts[j].amount);
                    //console.log("now", $scope.detail_impo.products[i].amount);
                    //console.log("source", detail_impo_source.products[i].amount);
                    obj_product = {
                        name: allProducts[j].name,
                        detail: allProducts[j].detail,
                        price_in: Math.round( ( ( parseInt(allProducts[j].price_in) *  parseInt(allProducts[j].amount) ) + ( parseInt($scope.detail_impo.products[i].price_in) * parseInt($scope.detail_impo.products[i].amount) ) - (parseInt(detail_impo_source.products[i].price_in) * parseInt(detail_impo_source.products[i].amount) ) )/( parseInt(allProducts[j].amount) + parseInt($scope.detail_impo.products[i].amount) - parseInt(detail_impo_source.products[i].amount) ) ),
                        price: allProducts[j].price,
                        amount: parseInt(allProducts[j].amount) + parseInt($scope.detail_impo.products[i].amount) - parseInt(detail_impo_source.products[i].amount),
                        version: allProducts[j].version,
                        vendor: allProducts[j].vendor,
                        image: allProducts[j].image,
                        code: allProducts[j].code,
                        sku: allProducts[j].sku,
                        status: allProducts[j].status                        
                    }
                    //console.log("Total:", obj_product.amount);
                    $http.put("/products/"+ allProducts[j]._id, obj_product).then(function(res){
                        console.log(res);
                    })
                    .catch((er)=>{
                        console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                        console.log(er);
                    })
                }

            }
        }
    }

    $scope.add_note = function(){
        jQuery("#order_note").show();
        setIdNoteOrder($scope.detail_order._id);
    }

    $scope.search_note = function(){
        $http.get("/search/notes?sourceId=" + $scope.detail_order._id + "&source=orders").then((a)=>{
            // console.log(a);
            $scope.detail_notes = a.data;
        }).catch((e)=>{
            console.log(e);
        })
    }

    $scope.print= function(){
        printJS({ 
            printable: 'order-form', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.printStock = function(){
        printJS({ 
            printable: 'print-check-product', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.printTransport = function(){
        printJS({ 
            printable: 'transport-form', 
            type: 'html', 
            // header: 'PrintJS - Form Element Selection',
            targetStyles: ['*'] 
        });
    }
    $scope.define_controller_fin = "5d9e98955b547c0c297868b3"; //id of ifttt
    $scope.define_status_fin = [
        {
            name: "Đã thanh toán",
            code: "da_thanh_toan"
        },
        {
            name: "Chưa thanh toán",
            code: "chua_thanh_toan"
        }
    ]
    $scope.finance = function(){
        var state_fin = 0;
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                // return;
                state_fin = 1;
            }
        }
        if(state_fin == 0){
            $scope.detail_order.total = $scope.getTotalI($scope.detail_order);
            $scope.detail_order.parent = "BAN_SI";
            $scope.detail_order.info_customer = "Đơn hàng lẻ: " + $scope.getCustomerName($scope.detail_order.customer) + " - " + $scope.detail_order._id;
            $http.post("/fin/create", $scope.detail_order).then((a)=>{
                $scope.detail_order.extend.push({
                    name: "finance",
                    idB: a.data._id,
                    _id: $scope.define_controller_fin
                });
                console.log(a);
            }).catch((e)=>{
                console.log(e);
            })
        } else if(state_fin == 1){
            $scope.updateOrder();
            let extend = {};
            console.log($scope.detail_order)
            for(i in $scope.detail_order.extend){
                if($scope.detail_order.extend[i].name == "finance"){
                    extend = $scope.detail_order.extend[i];
                }
            }
            extend.oA = {
                status: $scope.detail_order.status
            }
            $http.post("/fin/update", extend).then((a)=>{

            }).catch((e)=>{

            })
        }
    }
    $scope.definance = function(){
        var a = {};
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                a = $scope.detail_order.extend[i];
            }
        }
        $http.post("/fin/delete", a).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        for(i in $scope.detail_order.extend){
            if($scope.detail_order.extend[i].name == "finance"){
                // a = $scope.detail_order.extend[i];
                $scope.detail_order.extend.splice(i, 1);
                break;
            }
        }
    }

    $scope.deleteProduct = function(i){
        // console.log(i);
        if(confirm("Bạn đang xoá: " + i.name + "!. Quá trình này không thể phục hồi!. Tiếp tục")){

        } else {
            return;
        }
        // $http.get("/products/"+ i._id).then(function(res){
        //     let obj_product = res.data;

        // });
        for(j in $scope.detail_order.products){
            if($scope.detail_order.products[j].productId == i.productId){
                // console.log(i.productId);
                $scope.detail_order.products.splice(j, 1);
                break;
            }
        }
    }

    $scope.aproducts = [];
    $scope.loadProducts = function(){
        $http.get("/products").then((a)=>{
            $scope.aproducts = a.data;
        })
    }
    $scope.addProduct = function(){
        if($scope.aproduct == "" || $scope.aproduct == undefined){
            alert("Chọn sản phẩm trước khi thêm nhé!");
            return;
        }
        console.log($scope.aproduct);
        let p = JSON.parse($scope.aproduct);
        $scope.detail_order.products.push({
                    productId: p._id,
                    name: p.name,
                    amount: 1,
                    price: p.price,
                    discount: 0
                });
                jQuery("#id01").hide();
    }
    $scope.loadProducts();

    /* STATE */
    $scope.pre_state = function(){
        /* Các STEP không thể lùi lại của đơn hàng */
        if(($scope.detail_order.status == 3)){
            alert("Xin lỗi, trạng thái này không khả dụng!.");
            return;
        }
        if(confirm("Bạn muốn lùi trạng thái của đơn hàng này?")){

        } else {
            return;
        }
        if(parseInt($scope.detail_order.status) > 1){
            $scope.detail_order.status = parseInt($scope.detail_order.status) - 1;
        } else {
            $scope.detail_order.status = 1;
        }
        
        if (true){
            $scope.updateOrder();
            let time_arr = [1,1,1,1]
            $scope.createWork(time_arr[parseInt($scope.detail_order.status) - 1]);
        }
        
    }

    $scope.nex_state = function(){
        for(i in $scope.state_order.state){
            if($scope.detail_order.status == $scope.state_order.state[i].step){
                if($scope.state_order.state[i].completed){
                    alert("Đơn hàng này đã giao thành công!. Đừng phá hệ thống TÀO LAO.");
                    return;
                }
            }
        }
        if(confirm("Bạn muốn đẩy tới trạng thái của đơn hàng này?")){

        } else {
            return;
        }
        if(parseInt($scope.detail_order.status) >= 1){
            $scope.detail_order.status = parseInt($scope.detail_order.status) + 1;
        } else {
            $scope.detail_order.status = 1;
        }

        if($scope.detail_order.status == 3){
            if(confirm("Bạn đã sẵn sàng!, bước này sẽ gởi thông tin đến giao vận và tài chính xác nhận hoá đơn. Chúng ta không thể lùi lại!")){
                /* WEBHOOK */
                $http.post("/webhooks/order/finance/DEBT", $scope.detail_order).then((a)=>{
                    console.log(a.data);
                })
            } else {
                return;
            }
        } else if ($scope.detail_order.status == 5) {
            alert("Chúc mừng bạn một đơn hàng đã thành công");
        }
        $scope.updateOrder();
        createWork($scope.detail_order, $scope.orders);
    }

    /* CHECKBOX */
    $scope.checkFinance = function(){
        if($scope.detail_order.finance){
            if(confirm("Bạn có muốn ra một thông báo!")){
                console.log("Chúc mừng bạn!");
            } else {
                console.log("Cố gắng lên nha!");
            }
            $http.post("/webhooks/order/finance/PAY", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        } else {
            $http.post("/webhooks/order/finance/DELETE_CREATE", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        }
        $scope.updateOrder();
        
    }
    // $scope.reload()
    $scope.formatNum = formatNumber;
    /* RUN WHEN STARTUP */
    $scope.begin();
    jQuery("#all_order").show();
    jQuery("#detail_all_order").hide();
});
