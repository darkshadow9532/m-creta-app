app.controller("create_product", function($scope, $http, $interval, $timeout) {
    $scope.images = [{link: ""}]
    $scope.addImage = function(){
        console.log($scope.images)
        $scope.images.push( {link: ""});
    }

    $scope.create = function(){
        console.log($scope.name)
        if($scope.name == undefined){
            
            alert("Nhập tên sản phẩm");
            return;
        }
        let obj = {
            name: $scope.name,
            detail: $scope.detail,
            price_in: $scope.price_in,
            price: $scope.price,
            amount: 0,
            version: $scope.version,
            vendor: $scope.vendor,
            image: $scope.images,
            code: $scope.code,
            sku: $scope.sku
        }

        // let b = jQuery("button");
        // for(i in b){
        //     try{
        //         jQuery(b[i]).prop("disabled", true);
        //     } catch (e) {
        //         console.log(e);
        //         // continue;
        //     }
            
        // }
        // jQuery("#btnCreate").prop("disabled", true);
        $http.post("/products", obj).then((res)=>{  
            console.log(res);
            // let obj_stock = {
            //     productId: res.data._id,
            //     productName: res.data.name,
            //     amount: 0,
            //     a_amount: 0,
            //     price_in: res.data.price_in,
            //     schedule: []
            // }
            // $http.post("/stocks", obj_stock).then(function(res_stock){
            //     console.log(res_stock);
            // })
            // .catch((er)=>{
            //     console.log(er);
            // })
            //location.reload();
            // let b = jQuery("button");
            // try{
            //     jQuery(b[i]).prop("disabled", true);
            // } catch (e) {
            //     continue;
            // }
            // jQuery("#btnCreate").prop("disabled", false);
        }).catch((e)=>{
            console.log(e);
        })
    }
});

app.controller("all_product", function($scope, $http, $interval, $timeout) {
    $scope.products = [];
    $scope.detail_product = {};
    $scope.begin = function(){
        //GET ALL PRODUCTS
        $http.get("/products").then((a)=>{
            if(a.status == 200){
                for(i in a.data){
                    if(a.data[i].status == undefined){
                        a.data[i].status = {};
                    }
                }
                $scope.products = a.data;
                
            }
        }).catch((e)=>{

        })

    }

    $scope.detail = function(i){
        $scope.detail_product = i;
        jQuery("#detail_all_product").show();
        jQuery("#all_product").hide();
    }

    $scope.back = function(){
        jQuery("#detail_all_product").hide();
        jQuery("#all_product").show();
        $scope.begin();
    }

    $scope.delete = function(){
        if(confirm("Bạn đang muốn xoá sản phẩm: " + $scope.detail_product._id)){
            $http.delete("/products/" + $scope.detail_product._id).then((a)=>{
                if(a.status == 200){
                    runNotify("Bạn đã xoá sản phẩm", "yellow", 10);
                    location.reload();
                }
            }).catch((e)=>{
                runNotify("Bạn xoá sản phẩm thất bại", "red", 5);
            })
        } else {

        }
    }

    $scope.edit = function(){
        if(confirm("Bạn đang cập nhật thông tin sản phẩm")){
            $http.put("/products/" + $scope.detail_product._id, $scope.detail_product).then((a)=>{
                if(a.status == 200){
                    runNotify("Bạn đã cập nhật thành công", "green", 5);
                } else {
                    runNotify("Đã xảy ra một lỗi gì đó trong quá trình cập nhật", "yellow", 5);
                }

            }).catch((e)=>{
                runNotify("Cập nhật thất bại", "red", 5);
            })
        }
    }

    $scope.formatNumber = formatNumber;
    $scope.date = date;
    /* BEGIN */
    $scope.begin();
    jQuery("#detail_all_product").hide();
});