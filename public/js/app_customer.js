app.controller("all_customer", async function($scope, $http, $interval, $timeout) {
    $scope.customers = [];
    $scope.detail_customer = "";
    $scope.detail_notes = [];
    $scope.date = date;
    $scope.num = formatNumber;
    $scope.begin = async function(){
        let customer = new Customer();
        customer = await customer.find();
        customer.forEach(async function(e, i){
            let j = new Customer(e._id);
            await j.begin();
            //customer[i].total = await j.getTotal();
            $scope.$apply(()=>{
                $scope.customers = customer;
            })
        });

    }
    $scope.cutString = cutString;
    $scope.detail = function(i){
        location.replace(window.location.origin + "/views/customer/" + i._id);
    }
    $scope.back = function(str){
        jQuery('#' +str).hide();
        jQuery('#all_customer').show();
    }
    $scope.delete = function(id){
        $http.delete("/customers/" + id).then(async function(a){
            console.log(a);
            await $scope.begin();
        }).catch((e)=>{
            console.log(e);
        })
        
    }
    $scope.edit = function(){
        let obj = {
            id: $scope.detail_customer._id,
            name: $scope.detail_customer.name,
            first: $scope.detail_customer.first,
            email: $scope.detail_customer.email,
            phone: $scope.detail_customer.phone,
            note: $scope.detail_customer.note,
            address: $scope.detail_customer.address,
            transport: $scope.detail_customer.transport
        }
        $http.put("/customers/" + $scope.detail_customer._id, obj).then((a)=>{
            console.log(a);
            if(a.data){
                if(a.data._id){

                    $scope.cd_customer_alert = "Tạo thành công khách hàng " + a.data.name  + " " + a.data.first; 
                    return;  
                }    
            }

            $scope.cd_customer_alert = "Tạo khách hàng thất bại"
        }).catch((E)=>{
            console.log(E);
        })
    }
    $scope.add_note = function(){
        jQuery("#customer_note").show();
        setId($scope.detail_customer._id);
    }

    $scope.search_note = function(){
        $http.get("/search/notes?sourceId=" + $scope.detail_customer._id + "&source=customers").then((a)=>{
            // console.log(a);
            $scope.detail_notes = a.data;
        }).catch((e)=>{
            console.log(e);
        })
    }

    $scope.deleteNote = function(id){
        if(confirm("Bạn xoá một ghi chú, nó không thể phục hồi")){
            $http.delete("/notes/" + id).then((a)=>{
                if(a.status == 200){
                    runNotify("Bạn đã xoá thành công ghi chú", "orange", 5);
                    $scope.search_note();
                }
            }).catch((e)=>{
                runNotify("Có một lỗi gì đó xảy ra!. Làm lại đi", "red", 5);
            })
        } else {
            runNotify("Đừng phân vân thế chứ!. :D", "red", 10);
        }
    }
    /* BIEGIN WITH STARTUP */
    await $scope.begin();
    // $scope.back('detail_all_customer')
    jQuery('#detail_all_customer').hide();

});

app.controller("cr_customer", function($scope, $http, $interval, $timeout) {
    $scope.create = function(){
        let obj = {
            name: $scope.name,
            first: $scope.first,
            email: $scope.email,
            phone: $scope.phone,
            note: $scope.note,
            address: $scope.address,
            transport: $scope.transport
        }
        $http.post("/customers", obj).then((a)=>{
            console.log(a);
            if(a.data){
                if(a.data._id){

                    $scope.cd_customer_alert = "Tạo thành công khách hàng " + a.data.name  + " " + a.data.first; 
                    return;  
                }    
            }

            $scope.cd_customer_alert = "Tạo khách hàng thất bại"
        }).catch((E)=>{
            console.log(E);
        })
    }


});

// app.controller("detail_all_customer", function($scope, $http, $interval, $timeout) {

// });

function formatNumber(num) {
    let r;
    try{
        r = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } catch (e){
        r = "";
    }
    return r;
}

app.controller("show_customer", async function($scope, $http, $interval, $timeout){
    let u = window.location.href.split("/");
    let i = 0;
    $scope.element;
    $scope.total = 0;
    for(i in u){
        $scope.element = u[i];
    }
    
    $scope.getTotal = function(){
        let total = 0;
        // console.log(total)
        $scope.orders.forEach((a)=>{
            total += a.getMoney();
            // console.log(a.getMoney())
        })
        return total;
    }
    $scope.begin = async function(){
        $scope.c_element = new Customer($scope.element);
        await $scope.c_element.begin();
        let or = await $scope.c_element.getOrder();
        $scope.n_element = [];
        let tot = await $scope.c_element.getTotal();
        $scope.$apply(function(){
            $scope.orders = or;
            $scope.total = tot;
            $scope.d_element = $scope.c_element.get();
            $scope.n_element = $scope.c_element.getNote();
            // $scope.total = $scope.getTotal();
        });
    }

    $scope.num = formatNumber;
    await $scope.begin();
    $scope.delete = async function(){
        if(confirm("Bạn đang xoá!. Hành động này không thể phục hồi!")){
            await $scope.c_element.delete();
            location.replace(window.location.origin + "/views/customer/");
        } else {

        }
    }

    $scope.edit = function(){
        $scope.c_element.set($scope.d_element);
        $scope.c_element.save();
        alert("Đã lưu thành công!");
    }

    /* NOTE ELEMENT */
    $scope.n_element_note_delete = async function(note){
        if(confirm("Bạn đang xoá!. Hành động này không thể phục hồi!")){
            await note.delete();
            await $scope.begin();
            // location.replace(window.location.origin + "/views/customer/");
        } else {

        }
    }

    /* CRETA NOTE */
    jQuery("#n_note").hide();
    $scope.create_note = async function(){
        console.log($scope.n_element);
        jQuery("#n_note").show()
    }

    $scope.n_note_cancel = function(){
        $scope.n_note_title = "";
        $scope.n_note_content = "";
        jQuery("#n_note").hide();
    }

    $scope.n_note_add = async function(){
        let n = new Note({
            title: $scope.n_note_title,
            content: $scope.n_note_content,
            source: "customers",
            sourceId: $scope.d_element._id
        });
        let t_n = await n.begin();
        await n.save();
        $scope.$apply(()=>{
            $scope.n_element.push(n);
        })
        $scope.n_note_cancel();
    }
});