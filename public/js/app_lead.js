app.controller("all_lead", function($scope, $http, $interval, $timeout) {
    $scope.leads = [];
    $scope.detail_lead = "";
    $scope.detail_notes = [];
    $scope.date = date;
    $scope.begin = async function(){
        let l = new Lead();
        l = await l.find();
        $scope.$apply(function(){
            $scope.leads = l;
        });
    }
    $scope.cutString = cutString;
    $scope.detail = function(i){
        location.replace(window.location.origin + "/views/lead/" + i._id);
        // location.
        $scope.detail_lead = i;
        $scope.search_note();
        jQuery('#detail_all_lead').show();
        jQuery('#all_lead').hide();
    }
    $scope.back = function(str){
        jQuery('#' +str).hide();
        jQuery('#all_lead').show();
    }
    $scope.delete = function(id){
        $http.delete("/leads/" + id).then((a)=>{
            console.log(a);
            $scope.begin();
            $scope.back('detail_all_lead');
        }).catch((e)=>{
            console.log(e);
        })
        
    }
    $scope.edit = function(){
        let obj = {
            id: $scope.detail_lead._id,
            name: $scope.detail_lead.name,
            first: $scope.detail_lead.first,
            email: $scope.detail_lead.email,
            phone: $scope.detail_lead.phone,
            note: $scope.detail_lead.note,
            address: $scope.detail_lead.address,
            transport: $scope.detail_lead.transport
        }
        $http.put("/leads/" + $scope.detail_lead._id, obj).then((a)=>{
            console.log(a);
            if(a.data){
                if(a.data._id){

                    $scope.cd_lead_alert = "Tạo thành công khách hàng " + a.data.name  + " " + a.data.first; 
                    return;  
                }    
            }

            $scope.cd_lead_alert = "Tạo khách hàng thất bại"
        }).catch((E)=>{
            console.log(E);
        })
    }
    $scope.add_note = function(){
        jQuery("#lead_note").show();
        setId($scope.detail_lead._id);
    }

    $scope.search_note = function(){
        $http.get("/search/notes?sourceId=" + $scope.detail_lead._id + "&source=leads").then((a)=>{
            // console.log(a);
            $scope.detail_notes = a.data;
        }).catch((e)=>{
            console.log(e);
        })
    }

    $scope.deleteNote = function(id){
        if(confirm("Bạn xoá một ghi chú, nó không thể phục hồi")){
            $http.delete("/notes/" + id).then((a)=>{
                if(a.status == 200){
                    runNotify("Bạn đã xoá thành công ghi chú", "orange", 5);
                    $scope.search_note();
                }
            }).catch((e)=>{
                runNotify("Có một lỗi gì đó xảy ra!. Làm lại đi", "red", 5);
            })
        } else {
            runNotify("Đừng phân vân thế chứ!. :D", "red", 10);
        }
    }
    /* BIEGIN WITH STARTUP */
    $scope.begin();
    // $scope.back('detail_all_lead')
    jQuery('#detail_all_lead').hide();

});

app.controller("cr_lead", function($scope, $http, $interval, $timeout) {
    $scope.create = async function(){
        let obj = {
            name: $scope.name,
            first: $scope.first,
            email: $scope.email,
            phone: $scope.phone,
            note: $scope.note,
            address: $scope.address,
            transport: $scope.transport
        }
        let lead = new Lead(obj);
        let res = await lead.save();
        location.replace(window.location.origin + "/views/lead/" + lead.get()._id);
        console.log(lead.get());
    }


});

app.controller("show_lead", async function($scope, $http, $interval, $timeout){
    let u = window.location.href.split("/");
    let i = 0;
    $scope.element;
    for(i in u){
        $scope.element = u[i];
    }
    
    $scope.begin = async function(){
        $scope.c_element = new Lead($scope.element);
        await $scope.c_element.begin();
        $scope.n_element = [];
        // let n = await $scope.c_element.getNote();
        $scope.$apply(function(){
            $scope.d_element = $scope.c_element.get();
            $scope.n_element = $scope.c_element.getNote();
        });
    }
    // console.log($scope.element);
    await $scope.begin();
    // console.log($scope.d_element)
    $scope.delete = async function(){
        if(confirm("Bạn đang xoá!. Hành động này không thể phục hồi!")){
            await $scope.c_element.delete();
            location.replace(window.location.origin + "/views/lead/");
        } else {

        }
    }

    /* NOTE ELEMENT */
    $scope.n_element_note_delete = async function(note){
        if(confirm("Bạn đang xoá!. Hành động này không thể phục hồi!")){
            await note.delete();
            await $scope.begin();
            // location.replace(window.location.origin + "/views/lead/");
        } else {

        }
    }

    /* CRETA NOTE */
    jQuery("#n_note").hide();
    $scope.create_note = async function(){
        console.log($scope.n_element);
        jQuery("#n_note").show()
    }

    $scope.n_note_cancel = function(){
        $scope.n_note_title = "";
        $scope.n_note_content = "";
        jQuery("#n_note").hide();
    }

    $scope.n_note_add = async function(){
        let n = new Note({
            title: $scope.n_note_title,
            content: $scope.n_note_content,
            source: "leads",
            sourceId: $scope.d_element._id
        });
        let t_n = await n.begin();
        await n.save();
        $scope.$apply(()=>{
            $scope.n_element.push(n);
        })
        $scope.n_note_cancel();
    }
});