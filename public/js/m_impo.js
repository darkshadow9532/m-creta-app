// function numberfin(num){
//     return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(num)
// }
// var app = angular.module("order", []);
function find_Object(details, object_arr){
    let arr = {};
    for (i in object_arr){
        let temp = true;
        for (j in details){
            if(!object_arr[i][j]){
                temp = false;
            }
            else if ( object_arr[i][j] == details[j] ){
                temp = false;
            }             
        }
        if (temp) {
            arr.push(object_arr[i]);
        }
    }
    return arr;
}

var products = [];
var detail_impo_source = {};

app.controller("create_impo", function($scope, $http, $interval, $timeout) {
    $scope.products = [];
    $scope.aproducts = [];
    $scope.aproduct;
    $scope.customers = [];
    $scope.customer = "";
    $scope.staffs = [];
    $scope.staff = "";
    $scope.date = "";
    // $scope.create = function(){

    // }
    $http.get("/products").then((a)=>{
        // console.log(a);
        $scope.aproducts = a.data;
    })
    $scope.addProduct = function(){
        if($scope.aproduct == "" || $scope.aproduct == undefined){
            alert("Chọn sản phẩm trước khi thêm nhé!");
            return;
        }
        console.log($scope.aproduct);
        let p = JSON.parse($scope.aproduct);
        products.push(p);
        $scope.products.push({
            productId: p._id,
            name: p.name,
            amount: 1,
            price_in: p.price_in,
            price: p.price,
            discount: 0
        });
        // console.log($scope.aproduct);
        // if($scope.aproduct == "" || $scope.aproduct == undefined){
        //     alert("Chọn sản phẩm trước khi thêm nhé!");
        //     return;
        // }
        // // let temp = $scope.aproduct.split("---")[0];
        // // for(i in $scope.aproducts){
        //     // if($scope.aproducts[._id == temp){
        //         $scope.products.push({
        //             productId: $scope.aproduct._id,
        //             name: $scope.aproduct.name,
        //             amount: 1,
        //             price: $scope.aproduct.price,
        //             discount: 0
        //         });
            // }    
        // }
        // console.log("HEREx")
    }

    $scope.totalProductsOut = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    $scope.totalProductsIn = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price_in*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    // customer: String,
    // staff: String,
    // produtcs: Array,
    $scope.create = function(){
        var obj = {
            vendor: $scope.vendor,
            staffId: $scope.staff._id,
            products: $scope.products,
            date: $scope.date
        };
        $http.post("/impos", obj).then((a)=>{
            console.log(a);
        }).catch((e)=>{
            console.log(e);
        })
        //console.log("test");
        let obj_product = {}
        console.log($scope.products);
        for (j in products){            
            obj_product = {
                name: products[j].name,
                detail: products[j].detail,
                price_in: Math.round( ( ( parseInt(products[j].price_in) *  parseInt(products[j].amount) ) + ( parseInt($scope.products[j].price_in) * parseInt($scope.products[j].amount) ) )/( parseInt(products[j].amount) + parseInt($scope.products[j].amount) ) ),
                price: products[j].price,
                amount: parseInt(products[j].amount) + parseInt($scope.products[j].amount),
                version: products[j].version,
                vendor: products[j].vendor,
                image: products[j].image,
                code: products[j].code,
                sku: products[j].sku,
                status: products[j].status                        
            }
            $http.put("/products/"+ products[j]._id, obj_product).then(function(res){
                console.log(res);
            })
            .catch((er)=>{
                console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                console.log(er);
            })
        }
        products = [];
        $scope.products = [];
    }

    $http.get("/customers").then((a)=>{
        // console.log(a);
        $scope.customers = a.data;
    }).catch((e)=>{
        console.log(e);
    })
    $http.get("/staffs").then((a)=>{
        // console.log(a);
        $scope.staffs = a.data;
    }).catch((e)=>{
        console.log(e);
    })

    $http.get("/stocks").then(function(res){
        $scope.stocks = res.data;
    });
    $scope.addCustomer = function(){
        // console.log($scope.customer)
    }
});

app.controller("all_impo", function($scope, $http, $interval, $timeout) {
    $scope.impos = [];
    $scope.detail_impo = {};
    $scope.cutString = function(str, n){
        var b = str.length; 
        return str.slice(b-n, b);
    }
    $scope.totalProducts = function(){
        let total = 0;
        for(i in $scope.products){
            // i.price * i.amount - i.discount
            total += ($scope.products[i].price*$scope.products[i].amount - $scope.products[i].discount);
        }
        return total;
    }
    $scope.getTotalIn = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price_in) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.getTotalOut = function(order){
        let temp = 0;
        // console.log(order)
        for(i in order.products){
            // console.log(order.products[i])
            temp += ((parseInt(order.products[i].price) - order.products[i].discount)* order.products[i].amount)
        }
        return formatNumber(temp);
    }
    $scope.detail = function(i){
        console.log($scope.impos);
        $scope.detail_impo = i;
        $http.get("/impos/" + i._id).then(function(res){
            detail_impo_source = res.data;
        });
        // if($scope.detail_impo.finance == undefined){
        //     $scope.detail_impo = false;
        // }
        console.log(i);
        jQuery("#detail_all_impo").show();
        jQuery("#all_impo").hide();
    }

    $scope.back = function(){
        jQuery("#detail_all_impo").hide();
        jQuery("#all_impo").show();
    }

    $scope.delete = function(){       
        
        if(confirm("Bạn đang xoá một đơn nhập hàng?")){
            $http.delete("/impos/" + $scope.detail_impo._id).then((a)=>{
                runNotify("Bạn đã xoá đơn nhập hàng", "green", 5);                
                $scope.begin();
            }).catch((e)=>{
                runNotify("Có một lỗi xảy ra", "red", 5);
            })
            let obj_product = {}
            for (i in $scope.detail_impo.products){
                for (j in $scope.aproducts){
                    if( $scope.aproducts[j]._id == $scope.detail_impo.products[i].productId){
                        obj_product = {
                            name: $scope.aproducts[j].name,
                            detail: $scope.aproducts[j].detail,
                            price_in: Math.round( ( ( parseInt($scope.aproducts[j].price_in) *  parseInt($scope.aproducts[j].amount) ) - ( parseInt($scope.detail_impo.products[i].price_in) * parseInt($scope.detail_impo.products[i].amount) ) )/( parseInt($scope.aproducts[j].amount) - parseInt($scope.detail_impo.products[i].amount) ) ),
                            price: $scope.aproducts[j].price,
                            amount: parseInt($scope.aproducts[j].amount) - parseInt($scope.detail_impo.products[i].amount),
                            version: $scope.aproducts[j].version,
                            vendor: $scope.aproducts[j].vendor,
                            image: $scope.aproducts[j].image,
                            code: $scope.aproducts[j].code,
                            sku: $scope.aproducts[j].sku,
                            status: $scope.aproducts[j].status                        
                        }
                        $http.put("/products/"+ $scope.aproducts[j]._id, obj_product).then(function(res){
                            console.log(res);
                        })
                        .catch((er)=>{
                            console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                            console.log(er);
                        })
                    }

                }
            }
            $http.post("/webhooks/impo/finance/DELETE", $scope.detail_order).then((a)=>{
                console.log(a.data);
            })
        } else {

        }
    }
    $scope.checkFinance = function(){
        console.log("FUN:", $scope.detail_impo.finance);
        if($scope.detail_impo.finance == true){
            if(confirm("Bạn có muốn ra một thông báo!")){
                console.log("Chúc mừng bạn!");
            } else {
                console.log("Cố gắng lên nha!");
            }
            $http.post("/webhooks/impo/finance/PAY", $scope.detail_impo).then((a)=>{
                console.log(a.data);
            })
        } else if ($scope.detail_impo.finance == false){
            $http.post("/webhooks/impo/finance/DELETE_CREATE", $scope.detail_impo).then((a)=>{
                console.log(a.data);
            })
        } else {
            $scope.detail_impo.finance = false;
        }
        $scope.edit();
    }
    $scope.edit = function(){
        //console.log($scope.detail_impo);
        if(confirm("Bạn đang cập nhật thông tin đơn nhập!.")){
            $http.put("/impos/" + $scope.detail_impo._id, $scope.detail_impo).then((a)=>{              
                
                runNotify("Bạn đã cập nhật đơn nhập hàng", "green", 5);
            }).catch((e)=>{
                runNotify("Có lỗi xảy ra trong quá trình cập nhật", "red", 5);
            })
            let obj_product = {}
            for (i in $scope.detail_impo.products){
                for (j in $scope.aproducts){
                    if( $scope.aproducts[j]._id == $scope.detail_impo.products[i].productId){
                        console.log("all", $scope.aproducts[j].amount);
                        console.log("now", $scope.detail_impo.products[i].amount);
                        console.log("source", detail_impo_source.products[i].amount);
                        obj_product = {
                            name: $scope.aproducts[j].name,
                            detail: $scope.aproducts[j].detail,
                            price_in: Math.round( ( ( parseInt($scope.aproducts[j].price_in) *  parseInt($scope.aproducts[j].amount) ) + ( parseInt($scope.detail_impo.products[i].price_in) * parseInt($scope.detail_impo.products[i].amount) ) - (parseInt(detail_impo_source.products[i].price_in) * parseInt(detail_impo_source.products[i].amount) ) )/( parseInt($scope.aproducts[j].amount) + parseInt($scope.detail_impo.products[i].amount) - parseInt(detail_impo_source.products[i].amount) ) ),
                            price: $scope.aproducts[j].price,
                            amount: parseInt($scope.aproducts[j].amount) + parseInt($scope.detail_impo.products[i].amount) - parseInt(detail_impo_source.products[i].amount),
                            version: $scope.aproducts[j].version,
                            vendor: $scope.aproducts[j].vendor,
                            image: $scope.aproducts[j].image,
                            code: $scope.aproducts[j].code,
                            sku: $scope.aproducts[j].sku,
                            status: $scope.aproducts[j].status                        
                        }
                        console.log("Total:", obj_product.amount);
                        $http.put("/products/"+ $scope.aproducts[j]._id, obj_product).then(function(res){
                            console.log(res);
                        })
                        .catch((er)=>{
                            console.log("Lỗi khi đồng bộ hệ thống, vui lòng thông báo bộ phận kỹ thuật!");
                            console.log(er);
                        })
                    }

                }
            }
        } else {

        }
    }
    $scope.formatNumber = formatNumber;
    $scope.date = date;
    $scope.cutString = cutString;
    $scope.begin = function(){
        $http.get("/impos").then((a)=>{
            if(a.status == 200){
                $scope.impos = a.data;
            }
        }).catch((E)=>{
            console.log(E);
        })
        $http.get("/products").then((a)=>{
            // console.log(a);
            $scope.aproducts = a.data;
            //console.log($scope.aproducts);
        })
    }

    //
    $scope.begin();
    jQuery("#detail_all_impo").hide();
});