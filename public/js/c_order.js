function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}

class Order extends Model {
    constructor(obj){
        super(obj, "/orders");
    }
    getMoney(){
        return orderToTotal(this.obj);
    }
    async find_date(d, m, y){
        let t = m + "/" + d + "/" + y;
        t = (new Date(t)).getTime();
        return await this.find_time(t, t  + 86400000)
    }
}