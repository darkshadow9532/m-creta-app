app.controller("body_sidebar", function($scope, $http, $interval, $timeout) {
    $scope.arrayApp = ['all_impo','detail_all_impo','detail_all_product', 'all_product', 'create_note', 'all_note', 'detail_all_customer', 'all_order', 'create_order', 'all_customer', 'cr_customer', 'create_product', 'create_impo', 'all_stock', 'detail_all_order']
    $scope.displayApp = function(name){
        for(i in $scope.arrayApp){
            try{
                jQuery("#" + $scope.arrayApp[i]).hide();
            } catch (E){
                console.log("NONE FOUND: ", E);
                continue;
            }
        }
        try{
            jQuery("#" + name).show();
        } catch (E){
            console.log("NOT DISPLAY: ", E);
        }
    }
    $scope.displayApp("");
});