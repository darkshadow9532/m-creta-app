const Impo = require('../models/impo.model.js');
const Stock = require('../models/stock.model.js');
const Counter = require('../models/counter.models.js');
var counter = "impos";
// Create and Save a new impo
var request = require("request");
urlBuss = "https://thawing-taiga-94022.herokuapp.com"
urlFinan = "https://protected-retreat-78737.herokuapp.com"
function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}
function post(url, data){
    return new Promise(function(res, rej){
        request.post({
            url: url,
            form: data
        }, function(e,r,b){
            if(e){
                rej(e);
                return;
            } else {
                res(b);
            }
        })
    });
}
exports.create = (req, res) => {
    Counter.find({name: counter}).then((co)=>{
        let temp_count;
        let check = 0;
        if(co.length == 0){
            let t_count = new Counter({name: counter, counter: "25"});
            t_count.save().then(data => {
                console.log(data);
            }).catch((e)=>{
                console.log(e);
            })
            check = 1;
            temp_count = {name: counter, counter: "25"};
        } else {
            temp_count = co[0];
        }
        if(check == 0){
            temp_count.counter = (parseInt(temp_count.counter) + 1).toString();
            Counter.findByIdAndUpdate(temp_count._id, {name: temp_count.name, counter: temp_count.counter}, {new: true}).then((a)=>{
                console.log("A", a);
            }).catch((e)=>{
                console.log(e);
            })
        }
        const impo = new Impo({
            vendor: req.body.vendor,
            staff: req.body.staffId,
            products: req.body.products,
            id: temp_count.counter,
            finance: req.body.finace || false,
            date: req.body.date || new Date()
        });
        
        // Save impo in the database
        impo.save()
            .then(data => {
                console.log("DATA:", data._id);
                var b = data;
                let obj = {
                    "money": orderToTotal(b),
                    "parent": "BAN_SI",
                    "info": "NH: " + b.id,
                    "code": "bs_chi_1",
                    "source": "impos",
                    "source_url": "http://thawing-taiga-94022.herokuapp.com/impos/" + b._id,
                    "source_id": b._id.toString(),
                }
                console.log(obj);
                post(urlFinan + "/trans", obj).then((a)=>{
                    console.log(a);
                }).catch((e)=>{
                    console.log(e);
                })
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the impo."
                });
            });
    });
    
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Impo.find()
        .then(notes => {
            res.send(notes);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};

// Find a single impo with a impoId
exports.findOne = (req, res) => {
    Impo.findById(req.params.impoId)
        .then(impo => {
            if (!impo) {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            res.send(impo);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            return res.status(500).send({
                message: "Error retrieving impo with id " + req.params.impoId
            });
        });
};

// Update a impo identified by the impoId in the request
exports.update = (req, res) => {


    // Find impo and update it with the request body
    Impo.findByIdAndUpdate(req.params.impoId, {
            vendor: req.body.vendor,
            staff: req.body.staffId,
            products: req.body.products,
            finance: req.body.finance || false,
            id: req.body.id || "",
            date: req.body.date || new Date()
        }, {
            new: true
        })
        .then(impo => {
            if (!impo) {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            res.send(impo);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            return res.status(500).send({
                message: "Error updating impo with id " + req.params.impoId
            });
        });
};

// Delete a impo with the specified impoId in the request
exports.delete = (req, res) => {
    Impo.findByIdAndRemove(req.params.impoId)
        .then(impo => {
            if (!impo) {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            res.send({
                message: "impo deleted successfully!"
            });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "impo not found with id " + req.params.impoId
                });
            }
            return res.status(500).send({
                message: "Could not delete impo with id " + req.params.impoId
            });
        });
};