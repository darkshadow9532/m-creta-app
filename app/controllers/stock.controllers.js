const Stock = require('../models/stock.model.js');

// // Create and Save a new stock
// exports.create = (req, res) => {
//     // Validate request
//     console.log("===>",req.body)
//     if(!req.body.content) {
//         return res.status(400).send({
//             message: "stock content can not be empty"
//         });
//     }

//     // Create a stock
//     const stock = new Stock({
//         title: req.body.title || "Untitled stock", 
//         content: req.body.content,
//         depend: {
//             source: req.body.model || "",
//             sourceId: req.body.modelId || ""
//         }
//     });

//     // Save stock in the database
//     stock.save()
//     .then(data => {
//         res.send(data);
//     }).catch(err => {
//         res.status(500).send({
//             message: err.message || "Some error occurred while creating the stock."
//         });
//     });
// };

// // Retrieve and return all notes from the database.
// exports.findAll = (req, res) => {
//     Stock.find()
//     .then(notes => {
//         res.send(notes);
//     }).catch(err => {
//         res.status(500).send({
//             message: err.message || "Some error occurred while retrieving notes."
//         });
//     });
// };

// // Find a single stock with a noteId
// exports.findOne = (req, res) => {
//     Stock.findById(req.params.noteId)
//     .then(stock => {
//         if(!stock) {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });            
//         }
//         res.send(stock);
//     }).catch(err => {
//         if(err.kind === 'ObjectId') {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });                
//         }
//         return res.status(500).send({
//             message: "Error retrieving stock with id " + req.params.noteId
//         });
//     });
// };

// // Update a stock identified by the noteId in the request
// exports.update = (req, res) => {
// // Validate Request
// if(!req.body.content) {
//     return res.status(400).send({
//         message: "stock content can not be empty"
//     });
// }

// // Find stock and update it with the request body
// Stock.findByIdAndUpdate(req.params.noteId, {
//         title: req.body.title || "Untitled stock",
//         content: req.body.content,
//         depend: {
//             source: req.body.model || "",
//             sourceId: req.body.modelId || ""
//         }
//     }, {new: true})
//     .then(stock => {
//         if(!stock) {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });
//         }
//         res.send(stock);
//     }).catch(err => {
//         if(err.kind === 'ObjectId') {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });                
//         }
//         return res.status(500).send({
//             message: "Error updating stock with id " + req.params.noteId
//         });
//     });
// };

// // Delete a stock with the specified noteId in the request
// exports.delete = (req, res) => {
//     Stock.findByIdAndRemove(req.params.noteId)
//     .then(stock => {
//         if(!stock) {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });
//         }
//         res.send({message: "stock deleted successfully!"});
//     }).catch(err => {
//         if(err.kind === 'ObjectId' || err.name === 'NotFound') {
//             return res.status(404).send({
//                 message: "stock not found with id " + req.params.noteId
//             });                
//         }
//         return res.status(500).send({
//             message: "Could not delete stock with id " + req.params.noteId
//         });
//     });
// };

exports.create = (req, res) => {
    // Validate request
    console.log("===>",req.body)
    if(!req.body.productId) {
        return res.status(400).send({
            message: "productId can not be empty"
        });
    }

    // Create a stock
    const stock = new Stock({
        productId: req.body.productId,
        productName: req.body.productName,
        a_amount: req.body.a_amount,
        amount: req.body.amount,
        price_in: req.body.price_in,
        schedule: []
    });

    // Save stock in the database
    stock.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the stock."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Stock.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single stock with a noteId
exports.findOne = (req, res) => {
    Stock.findById(req.params.noteId)
    .then(stock => {
        if(!stock) {
            return res.status(404).send({
                message: "stock not found with id " + req.params.noteId
            });            
        }
        res.send(stock);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "stock not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving stock with id " + req.params.noteId
        });
    });
};

// Update a stock identified by the noteId in the request
exports.update = (req, res) => {
// Validate Request
if(!req.body.productId) {
    return res.status(400).send({
        message: "productId can not be empty"
    });
}

// Find stock and update it with the request body
Stock.findByIdAndUpdate(req.params.stockId, {
        productId: req.body.productId,
        productName: req.body.productName,
        a_amount: req.body.a_amount,
        amount: req.body.amount,
        price_in: req.body.price_in,
        schedule: []
    }, {new: true})
    .then(stock => {
        if(!stock) {
            return res.status(404).send({
                message: "stock not found with id " + req.params.stockId
            });
        }
        res.send(stock);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "stock not found with id " + req.params.stockId
            });                
        }
        return res.status(500).send({
            message: "Error updating stock with id " + req.params.stockId
        });
    });
};

// Delete a stock with the specified noteId in the request
exports.delete = (req, res) => {
    Stock.findByIdAndRemove(req.params.stockId)
    .then(stock => {
        if(!stock) {
            return res.status(404).send({
                message: "stock not found with id " + req.params.stockId
            });
        }
        res.send({message: "stock deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "stock not found with id " + req.params.stockId
            });                
        }
        return res.status(500).send({
            message: "Could not delete stock with id " + req.params.stockId
        });
    });
};