const Lead = require('../models/lead.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    console.log("===>",req.body)
    // if(!req.body.content) {
    //     return res.status(400).send({
    //         message: "Note content can not be empty"
    //     });
    // }

    // Create a Note
    const lead = new Lead({
        name: req.body.name,
        first: req.body.first || "",
        phone: req.body.phone,
        email: req.body.email,
        note: req.body.note || '',
        address: req.body.address || "",
        transport: req.body.transport || '',
        // title: req.body.title || "Untitled Note", 
        // content: req.body.content
    });

    // Save Note in the database
    lead.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Lead.find()
    .then(leads => {
        res.send(leads);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    console.log(req.params)
    Lead.findById(req.params.leadId)
    .then(lead => {
        if(!lead) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });            
        }
        res.send(lead);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.leadId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
// Validate Request
// if(!req.body.content) {
//     return res.status(400).send({
//         message: "Note content can not be empty"
//     });
//}

// Find note and update it with the request body
Lead.findByIdAndUpdate(req.params.leadId, {
    name: req.body.name,
    first: req.body.first || "",
    phone: req.body.phone,
    email: req.body.email,
    note: req.body.note || '',
    address: req.body.address || "",
    transport: req.body.transport || '',
    }, {new: true})
    .then(lead => {
        if(!lead) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });
        }
        res.send(lead);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.leadId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Lead.findByIdAndRemove(req.params.leadId)
    .then(lead => {
        if(!lead) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.leadId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.leadId
        });
    });
};