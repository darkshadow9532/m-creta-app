const Order = require('../models/order.model.js');
var counter = "order";
const Counter = require('../models/counter.models.js');
// https://protected-retreat-78737.herokuapp.com/create/tran
// Create and Save a new order
exports.create = (req, res) => {
    Counter.find({name: counter}).then((co)=>{
        let temp_count;
        let check = 0;
        if(co.length == 0){
            let t_count = new Counter({name: counter, counter: "37"});
            t_count.save().then(data => {
                console.log(data);
            }).catch((e)=>{
                console.log(e);
            })
            check = 1;
            temp_count = {name: counter, counter: "37"};
        } else {
            temp_count = co[0];
        }
        if(check == 0){
            temp_count.counter = (parseInt(temp_count.counter) + 1).toString();
            Counter.findByIdAndUpdate(temp_count._id, {name: temp_count.name, counter: temp_count.counter}, {new: true}).then((a)=>{
                console.log("A", a);
            }).catch((e)=>{
                console.log(e);
            })
        }
        const order = new Order({
            customer: req.body.customerId,
            staff: req.body.staffId,
            products: req.body.products,
            status: req.body.status || "Chưa có trạng thái",
            extend: req.body.extend || [],
            id: temp_count.counter,
            finance: req.body.finance || false,
            date: req.body.date || new Date()
        });
    
        // Save order in the database
        order.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the order."
            });
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Order.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single order with a orderId
exports.findOne = (req, res) => {
    Order.findById(req.params.orderId)
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });            
        }
        res.send(order);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving order with id " + req.params.orderId
        });
    });
};

// Update a order identified by the orderId in the request
exports.update = (req, res) => {
    Order.findByIdAndUpdate(req.params.orderId, {
        customer: req.body.customer,
        staff: req.body.staff,
        products: req.body.products,
        status: req.body.status || "Chưa có trạng thái",
        extend: req.body.extend || [],
        finance: req.body.finance || false,
        id: req.body.id || "",
        date: req.body.date || new Date()
    }, {new: true})
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });
        }
        res.send(order);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Error updating order with id " + req.params.orderId
        });
    });
};

// Delete a order with the specified orderId in the request
exports.delete = (req, res) => {
    Order.findByIdAndRemove(req.params.orderId)
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });
        }
        res.send({message: "order deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Could not delete order with id " + req.params.orderId
        });
    });
};