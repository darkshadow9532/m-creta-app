const Staff = require('../models/staff.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    console.log("===>",req.body)
    // if(!req.body.content) {
    //     return res.status(400).send({
    //         message: "Note content can not be empty"
    //     });
    // }

    // Create a Note
    const staff = new Staff({
        name: req.body.name,
        first: req.body.first || "",
        phone: req.body.phone,
        email: req.body.email,
        note: req.body.note || '',
        pass: req.body.pass
        // title: req.body.title || "Untitled Note", 
        // content: req.body.content
    });

    // Save Note in the database
    staff.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Staff.find()
    .then(customers => {
        res.send(customers);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    console.log(req.params)
    Staff.findById(req.params.staffId)
    .then(staff => {
        if(!staff) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.staffId
            });            
        }
        res.send(staff);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.staffId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.staffId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
// Validate Request
// if(!req.body.content) {
//     return res.status(400).send({
//         message: "Note content can not be empty"
//     });
//}

// Find note and update it with the request body
Staff.findByIdAndUpdate(req.params.staffId, {
    name: req.body.name,
    first: req.body.first || "",
    phone: req.body.phone,
    email: req.body.email,
    note: req.body.note || '',
    pass: req.body.pass
    }, {new: true})
    .then(staff => {
        if(!staff) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.staffId
            });
        }
        res.send(staff);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.staffId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.staffId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Staff.findByIdAndRemove(req.params.noteId)
    .then(staff => {
        if(!staff) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};