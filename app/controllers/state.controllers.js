const State = require('../models/state.models.js');

// Create and Save a new State
exports.create = (req, res) => {
    const state = new State({
        code: req.body.code || "",
        name: req.body.name || "",
        state: req.body.state || [],
        info: req.body.info || ""       
    });

    // Save State in the database
    state.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the State."
        });
    });
};

// Retrieve and return all states from the database.
exports.findAll = (req, res) => {
    State.find()
    .then(states => {
        res.send(states);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving states."
        });
    });
};

exports.search = (req, res) => {
    var a = req.query;
    console.log(a);
    State.find(a).then((state)=>{
        res.send(state);
    }).catch((e)=>{
        return res.status(404).send({
            message: "NOTE FOUND"
        });
    })
}

// Find a single state with a stateId
exports.findOne = (req, res) => {
    State.findById(req.params.stateId)
    .then(state => {
        if(!state) {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });            
        }
        res.send(state);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving state with id " + req.params.stateId
        });
    });
};

// Update a state identified by the stateId in the request
exports.update = (req, res) => {

// Find state and update it with the request body
State.findByIdAndUpdate(req.params.stateId, {
    code: req.body.code || "",
    name: req.body.name || "",
    state: req.body.state || [],
    info: req.body.info || ""
    }, {new: true})
    .then(state => {
        if(!state) {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });
        }
        res.send(state);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });                
        }
        return res.status(500).send({
            message: "Error updating state with id " + req.params.stateId
        });
    });
};

// Delete a state with the specified stateId in the request
exports.delete = (req, res) => {
    State.findByIdAndRemove(req.params.stateId)
    .then(state => {
        if(!state) {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });
        }
        res.send({message: "State deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "State not found with id " + req.params.stateId
            });                
        }
        return res.status(500).send({
            message: "Could not delete state with id " + req.params.stateId
        });
    });
};