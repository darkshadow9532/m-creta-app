module.exports = (app) => {
    const staff = require('../controllers/staff.controller.js');
    const Staff = require("../models/staff.model.js");
    // Create a new Note
    app.post('/staffs', staff.create);

    // Retrieve all Notes
    app.get('/staffs', staff.findAll);

    // Retrieve a single Note with noteId
    app.get('/staffs/:staffId', staff.findOne);

    // Update a Note with noteId
    app.put('/staffs/:staffId', staff.update);

    // Delete a Note with noteId
    app.delete('/staffs/:staffId', staff.delete);
    var a = "staff";
    app.get("/views/" + a, function(req, res){
        res.render(a + "/all")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })

    app.post("/login", function(req, res){
        console.log(req.body);
        Staff.find({email: req.body.email}).then((staff)=>{
            console.log(staff);
            if(staff.length == 1){
                if(staff[0].pass == req.body.pass){
                    res.send({
                        status: "OK"
                    })                    
                } else {
                    res.send({
                        status: "FAIL"
                    })
                }
            } else {
                res.send({
                    status: "FAIL"
                })
            }
        }).catch((e)=>{
            console.log(e);
        })
    })

    app.get("/login", (req, res)=>{
        res.render("app/login");
    })
}