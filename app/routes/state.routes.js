module.exports = (app) => {
    const states = require('../controllers/state.controllers.js');

    // Create a new Note
    app.post('/states', states.create);

    // Retrieve all Notes
    app.get('/states', states.findAll);

    // Retrieve a single Note with stateId
    app.get('/states/:stateId', states.findOne);

    // Update a Note with stateId
    app.put('/states/:stateId', states.update);

    // Delete a Note with stateId
    app.delete('/states/:stateId', states.delete);

    app.get('/search/states', states.search)
}