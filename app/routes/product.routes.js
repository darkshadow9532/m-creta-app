module.exports = (app) => {
    const products = require('../controllers/product.controller.js');

    // Create a new Note
    app.post('/products', products.create);

    // Retrieve all products
    app.get('/products', products.findAll);

    // Retrieve a single Note with productId
    app.get('/products/:productId', products.findOne);

    // Update a Note with productId
    app.put('/products/:productId', products.update);

    // Delete a Note with productId
    app.delete('/products/:productId', products.delete);
    var a = "product";
    app.get("/views/" + a, function(req, res){
        res.render(a + "/all")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })

    app.get("/views/stock", function(req, res){
        res.render("stock/index")
    })
}