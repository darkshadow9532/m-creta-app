module.exports = (app) => {
    const customer = require('../controllers/customer.controller.js');

    // Create a new Note
    app.post('/customers', customer.create);

    // Retrieve all Notes
    app.get('/customers', customer.findAll);

    // Retrieve a single Note with noteId
    app.get('/customers/:customerId', customer.findOne);

    // Update a Note with noteId
    app.put('/customers/:customerId', customer.update);

    // Delete a Note with noteId
    app.delete('/customers/:customerId', customer.delete);

    var a = "customer";
    app.get("/views/" + a, function(req, res){
        res.render(a + "/all")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })
    app.get("/views/" + a + "/:id", function(req, res){
        res.render(a + "/show")
    })
    app.get("/views/customer_explore",function(req,res){
        res.render("customer_explore/all");
    })
    app.get("/views/customer_explore/:id", function(req,res){
        res.render("customer_explore/customer_exp")
    })
}