module.exports = (app) => {
    const lead = require('../controllers/lead.controller.js');

    // Create a new Note
    app.post('/leads', lead.create);

    // Retrieve all Notes
    app.get('/leads', lead.findAll);

    // Retrieve a single Note with noteId
    app.get('/leads/:leadId', lead.findOne);

    // Update a Note with noteId
    app.put('/leads/:leadId', lead.update);

    // Delete a Note with noteId
    app.delete('/leads/:leadId', lead.delete);

    var a = "lead";
    app.get("/views/" + a, function(req, res){
        res.render(a + "/all")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })
    app.get("/views/lead/:id", function(req, res){
        res.render(a + "/show")
    })
}