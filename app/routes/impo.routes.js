module.exports = (app) => {
    const impos = require('../controllers/impo.controller.js');

    // Create a new impo
    app.post('/impos', impos.create);

    // Retrieve all impos
    app.get('/impos', impos.findAll);

    // Retrieve a single impo with impoId
    app.get('/impos/:impoId', impos.findOne);

    // Update a impo with impoId
    app.put('/impos/:impoId', impos.update);

    // Delete a impo with impoId
    app.delete('/impos/:impoId', impos.delete);

    var a = "impo";
    app.get("/views/" + a, function(req, res){
        res.render(a + "/all")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })
    app.get("/views/" + a + "/:id", function(req, res){
        res.render(a + "/show")
    })
}