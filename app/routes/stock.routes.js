module.exports = (app) => {
    const stocks = require('../controllers/stock.controllers.js');

    // Create a new stock
    app.post('/stocks', stocks.create);

    // Retrieve all stocks
    app.get('/stocks', stocks.findAll);

    // Retrieve a single stock with stockId
    app.get('/stocks/:stockId', stocks.findOne);

    // Update a stock with stockId
    app.put('/stocks/:stockId', stocks.update);

    // Delete a stock with stockId
    app.delete('/stocks/:stockId', stocks.delete);

    var a = "stock";
    app.get("/views/stock", function(req, res){
        res.render("stock/index")
    })
    app.get("/create/" + a, (req, res)=>{
        res.render(a + "/create")
    })

    app.get("/views/m_stock", function(req, res){
        res.render("m_stock/index")
    })
}