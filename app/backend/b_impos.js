function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}

function orderAllTotal(os){
    let total = 0;
    for(i in os){
        total += orderToTotal(os[i]);
    }
    return total;
}
module.exports = (app) => {
    // request.post({url:'http://service.com/upload', form: {key:'value'}}, function(err,httpResponse,body){ /* ... */ }
    const request = require("request");
    /* BACKEND CONTROLLER */
    /* Running NEXT STATE */
    app.post("/webhooks/impo/state/:state", function(req, res){
        res.send({
            status:"OK",
        })
        return;
        console.log("REQ:", req.body);
    });
    app.post("/webhooks/impo/finance/:control", function(req, res){
        res.send({
            status:"OK",
        })
        return;
        var urlFinace = "https://protected-retreat-78737.herokuapp.com";
        // var urlFinace = "http://localhost:5005"
        var uriFinance = "/trans"
        var code_debt = "bs_chi_1";
        var code_pay = "bs_chi_2";
        console.log("FINANCE:", req.body.products);
        console.log("FINANCE_params:", req.params);
        var b = req.body;
        var a = req.params;
        switch(a.control){
            case "DEBT":{
                let obj = {
                    "money": orderToTotal(b),
                    "parent": "BAN_SI",
                    "info": "NH: " + b.id,
                    "code": code_debt,
                    "source": "impos",
                    "source_url": "http://thawing-taiga-94022.herokuapp.com/impos/" + b._id,
                    "source_id": b._id,
                }
                console.log("DEBT:",obj);
                request.post({url:urlFinace + uriFinance, form: obj}, function(err,httpResponse,body){ 
                    /* ... */ 
                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    res.send(body);
                });
            } break;
            case "PAY":{
                let obj = {
                    "money": orderToTotal(b),
                    "parent": "BAN_SI",
                    "info": "NH: " + b.id,
                    "code": code_pay,
                    "source": "impos",
                    "source_url": "http://thawing-taiga-94022.herokuapp.com/impos/" + b._id,
                    "source_id": b._id,
                }
                console.log("PAY:",obj);
                request.post({url:urlFinace + uriFinance, form: obj}, function(err,httpResponse,body){ 
                    /* ... */ 
                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    res.send(body);
                });
            } break;
            case "DELETE":{
                console.log(urlFinace + "/search/trans?source=impos&source_id=" + b._id);
                request.get({url:urlFinace + "/search/trans?source=impos&source_id=" + b._id}, function(err,httpResponse,bod){ 
                    /* ... */ 

                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    let body = JSON.parse(bod);
                    console.log(body);
                    for(j in body){
                        request.delete({url: urlFinace + uriFinance + "/" + body[j]._id}, function(e,r,b){
                            if(e){
                                // res.send("ERROR");
                                return;
                            }
                            console.log(b);
                            console.log("SUCCESS");
                        });
                    }
                    res.send("SUCCESS");
                });
            } break;
            case "DELETE_CREATE":{
                request.get({url:urlFinace + "/search/trans?source=impos&source_id=" + b._id + "&code=" + code_pay}, function(err,httpResponse,bod){ 
                    /* ... */ 

                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    let body = JSON.parse(bod);
                    console.log(body);
                    for(j in body){
                        request.delete({url: urlFinace + uriFinance + "/" + body[j]._id}, function(e,r,b){
                            if(e){
                                // res.send("ERROR");
                                return;
                            }
                            console.log(b);
                            console.log("SUCCESS");
                        });
                    }
                    res.send("SUCCESS");
                });
            } break;
            default:{
                res.send({
                    status: "ERROR",
                    data: "cannot found action in here"
                })
            }
        }
        
    });
}