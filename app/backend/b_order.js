function orderToTotal(order){
    let total = 0;
    // console.log(order.products);
    for(j in order.products){
        total += (order.products[j].price - order.products[j].discount)*order.products[j].amount;
    }
    return total;
}

function orderAllTotal(os){
    let total = 0;
    for(i in os){
        total += orderToTotal(os[i]);
    }
    return total;
}
module.exports = (app) => {
    // request.post({url:'http://service.com/upload', form: {key:'value'}}, function(err,httpResponse,body){ /* ... */ }
    const request = require("request");
    /* BACKEND CONTROLLER */
    /* Running NEXT STATE */
    app.post("/webhooks/order/state/:state", function(req, res){
        res.send({
            status:"OK",
        })
        return;
        console.log("REQ:", req.body);
    });
    app.post("/webhooks/order/finance/:control", function(req, res){
        res.send({
            status:"OK",
        })
        return;
        var urlFinace = "https://protected-retreat-78737.herokuapp.com";
        var uriFinance = "/trans"
        var code_debt = "bs_thu_1";
        var code_pay = "bs_thu_2";
        console.log("FINANCE:", req.body.products);
        console.log("FINANCE_params:", req.params);
        var b = req.body;
        var a = req.params;
        switch(a.control){
            case "DEBT":{
                let obj = {
                    "money": orderToTotal(b),
                    "parent": "BAN_SI",
                    "info": "BS: " + b.id,
                    "code": code_debt,
                    "source": "orders",
                    "source_url": "http://thawing-taiga-94022.herokuapp.com/orders/" + b._id,
                    "source_id": b._id,
                }
                request.post({url:urlFinace + uriFinance, form: obj}, function(err,httpResponse,body){ 
                    /* ... */ 
                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    res.send(body);
                });
            } break;
            case "PAY":{
                let obj = {
                    "money": orderToTotal(b),
                    "parent": "BAN_SI",
                    "info": "BS: " + b.id,
                    "code": code_pay,
                    "source": "orders",
                    "source_url": "http://thawing-taiga-94022.herokuapp.com/orders/" + b._id,
                    "source_id": b._id,
                }
                request.post({url:urlFinace + uriFinance, form: obj}, function(err,httpResponse,body){ 
                    /* ... */ 
                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    res.send(body);
                });
            } break;
            case "DELETE":{
                request.get({url:urlFinace + "/search/trans?source=orders&source_id=" + b._id}, function(err,httpResponse,bod){ 
                    /* ... */ 

                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    let body = JSON.parse(bod);
                    console.log(body);
                    for(j in body){
                        request.delete({url: urlFinace + uriFinance + "/" + body[j]._id}, function(e,r,b){
                            if(e){
                                // res.send("ERROR");
                                return;
                            }
                            console.log(b);
                            console.log("SUCCESS");
                        });
                    }
                    res.send("SUCCESS");
                });
            } break;
            case "DELETE_CREATE":{
                request.get({url:urlFinace + "/search/trans?source=orders&source_id=" + b._id + "&code=" + code_pay}, function(err,httpResponse,bod){ 
                    /* ... */ 

                    if(err){
                        console.log("ERROR - WHEN CREATE FINCANCE", err)
                        res.send("ERROR");
                        return;
                    }
                    let body = JSON.parse(bod);
                    console.log(bod);
                    for(j in body){
                        request.delete({url: urlFinace + uriFinance + "/" + body[j]._id}, function(e,r,b){
                            if(e){
                                // res.send("ERROR");
                                return;
                            }
                            console.log(b);
                            console.log("SUCCESS");
                        });
                    }
                    res.send("SUCCESS");
                });
            } break;
            default:{
                res.send({
                    status: "ERROR",
                    data: "cannot found action in here"
                })
            }
        }
        
    });
}