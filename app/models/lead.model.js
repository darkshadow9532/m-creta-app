const mongoose = require('mongoose');

const LeadSchema = mongoose.Schema({
    name: String,
    first: String,
    phone: String,
    email: String,
    note: String,
    address: String,
    transport: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Lead', LeadSchema);