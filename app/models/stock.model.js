const mongoose = require('mongoose');

// var element_shedule = {
//     id: String,
//     type: String
// }

const StockSchema = mongoose.Schema({
    productId: String,
    productName: String,
    a_amount: Number,
    amount: Number,
    price_in: String,
    shedule: Array
}, {
    timestamps: true
});

module.exports = mongoose.model('Stock', StockSchema);