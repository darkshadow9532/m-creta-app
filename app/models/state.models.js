const mongoose = require('mongoose');

const StateSchema = mongoose.Schema({
    code: String,
    name: String,
    info: String,
    state: Array
}, {
    timestamps: true
});

module.exports = mongoose.model('State', StateSchema);