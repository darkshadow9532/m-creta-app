const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    customer: String,
    staff: String,
    products: Array,
    status: String,
    finance: Boolean,
    id: String,
    extend: Array,
    date: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Order', OrderSchema);