const mongoose = require('mongoose');

const ImpoSchema = mongoose.Schema({
    vendor: String,
    staff: String,
    products: Array,
    id: String,
    finance: Boolean,
    date: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Impo', ImpoSchema);