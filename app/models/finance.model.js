const mongoose = require('mongoose');

const FinanceSchema = mongoose.Schema({
    name: String,
    first: String,
    phone: String,
    email: String,
    note: Array
}, {
    timestamps: true
});

module.exports = mongoose.model('Finance', FinanceSchema);