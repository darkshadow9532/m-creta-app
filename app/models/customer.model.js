const mongoose = require('mongoose');

const CustomerSchema = mongoose.Schema({
    name: String,
    first: String,
    phone: String,
    email: String,
    note: String,
    address: String,
    transport: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Customer', CustomerSchema);