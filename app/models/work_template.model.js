const mongoose = require('mongoose');

const WorkTemplateSchema = mongoose.Schema({
    // title: String,
    // content: String
    action: Array,
    time: Number,
    parent: String,
    parentId: String,
    name: String,
    message: String
}, {
    timestamps: true
});

module.exports = mongoose.model('WorkTemplate', WorkTemplateSchema);