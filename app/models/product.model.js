const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    name: String,
    detail: String,
    price_in: String,
    price: String,
    amount: Number,
    version: String,
    vendor: String,
    image: Array,
    code: String,
    sku: String,
    status: Object
}, {
    timestamps: true
});

module.exports = mongoose.model('Product', ProductSchema);