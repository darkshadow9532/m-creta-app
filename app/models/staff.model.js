const mongoose = require('mongoose');

const StaffSchema = mongoose.Schema({
    name: String,
    first: String,
    phone: String,
    email: String,
    note: Array, 
    pass: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Staff', StaffSchema);