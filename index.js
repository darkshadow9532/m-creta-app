const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
var bodyParser = require('body-parser');
var route = require("./route");
var restful = require('node-restful');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uri = "mongodb+srv://fin:asrkpvg7@cluster0-ogucd.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");
    // mongoose.close();
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("h");
});

var app = express();
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
app.get('/', route.mainPage)
app.get('/acustomer', route.acustomer)
app.get('/customer', route.customer)
app.get('/cus/:id', route.cus)
app.post('/customer', route.createCustomer)
app.post('/delete', route.delete)
app.post('/anote', route.anote);
app.get('/show/:page', route.show);
app.get("/corder", route.corder);
app.get("/cimpo", route.cimpo);
app.get("/app/:func", route.app);
app.get("/app", route.app);
app.get("/test", route.test)
app.get("/black", (req, res)=>{
    res.render("app/index")
})

app.get("/sale", (req, res)=>{
    res.render("person/sale")
})

// Minh
app.get("/update_product", route.update_product);
require('./app/routes/mqtt.routes.js')(app);
require('./app/routes/work_template.routes.js')(app);
require('./app/routes/work.routes.js')(app);
// Minh end


require('./app/routes/note.routes.js')(app);
require('./app/routes/customer.routes.js')(app);
require('./app/routes/product.routes.js')(app);
require('./app/routes/staff.routes.js')(app);
require('./app/routes/order.routers.js')(app);
require('./app/routes/impo.routes.js')(app);
require('./app/routes/fin.routes.js')(app);
require('./app/routes/lead.routes.js')(app);
require('./app/backend/b_order.js')(app);
require('./app/backend/b_impos.js')(app);
require('./app/routes/out.routes.js')(app);
require('./app/routes/stock.routes.js')(app);

// Minh begin

var moment = require('moment');
moment.locale("vi");
moment().format();

// listen for requests

const mqtt = require('async-mqtt');

var server  = mqtt.connect('mqtt://cretabase.kbvision.tv:1883');

server.on('connect', function () {
    server.subscribe('minh', function (err) {
        if(!err) {
            client.publish('minh','hello');
        }
    })
})

server.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString())
    //server.end()
});

const Works = require('./app/models/work.model.js');
const MQTTs = require('./app/models/mqtt.model.js');
function deadline_check(){
    console.log("Starting...")
    let query_work = Works.find();
    let query_mqtt = MQTTs.find();
    let promise_work = query_work.exec();
    let promise_mqtt = query_mqtt.exec();
    Promise.all(
        [
            Promise.resolve(promise_work),
            Promise.resolve(promise_mqtt)
        ]
    )
    .then(function(result){
        //console.log(result);
        let works = result[0];
        let works_true = [];
        for (let i in works){
            if (Math.round(moment().diff(works[i].time, 'minutes', true)) === 0){
                works_true.push(works[i]);
            }
        }
        console.log("works_true:", works_true);
        for (let i in works_true){
            
            let mqttId = works_true[i].action;
            var mqtt_client;
            let mqtts = result[1];
            console.log("mqttId",mqttId);
            //  console.log("mqtts", mqtts);

            
            for (let j in mqttId){
                for (let k in mqtts){
                    if (mqtts[k]._id == mqttId[j]){
                        let client_info = "";
                        console.log(mqtts[k].port);
                        if (mqtts[k].port != ""){
                            
                            client_info = "mqtt://" + mqtts[k].host + ":" + mqtts[k].port;

                        }
                        else {
                            client_info  = "mqtt://" + mqtts[k].host;
                        }
                        console.log(client_info);
                        let client  = mqtt.connect(client_info);
                        //console.log(client);
                        client.on('connect', function () {
                            console.log('connect');                            
                            client.publish(mqtts[k].topic, mqtts[k].message);
                            console.log(mqtts[k].topic, mqtts[k].message);
                            client.end();
                        });
                    }
                }             
                    
            }
                
        }
    })
    
    .catch(err => {
        console.log(err);
        // res.status(500).send({
        //     message: err.message || "Something went wrong"
        // });
    });
}

setInterval(deadline_check, 60000);

// Minh end

// require('./app/routes/state.routes.js')(app);
require('./app/routes/state.routes.js')(app);
app.listen(PORT);